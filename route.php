<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Torta Route</title>

    <link href="torta_html_lego/assets/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="torta_html_lego/assets/bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
    <style media="screen">
      div.container {
        margin-top: 24px;
      }
    </style>
  </head>
  <body>

    <div class="container">

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Torta Html</h3>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12">
              <h2>Torta Development</h2>
              <ul class="list-group exp-elements">
                <li class="list-group-item">
                  <a href="torta_html_lego/template/sites/home.php">Website</a>
                </li>
                <li class="list-group-item">
                  <a href="torta_html_documentation/index.html">Documentation</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <h2>Torta Experimentals</h2>
              <ul class="list-group exp-elements">
                <li class="list-group-item">
                  <a href="torta_html_lego/template/experimentals/scrollrevealjs/index.php">scrollrevealjs</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <h2>Torta Production</h2>
              <ul class="list-group exp-elements">
                <li class="list-group-item">
                  <a href="torta_html_lego/phantom/private/home.html">Private</a>
                </li>
                <li class="list-group-item">
                  <a href="torta_html_lego/phantom/public/home.html">Public</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Torta Psd</h3>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12">
              <h2>Torta Lego</h2>
              <ul class="list-group exp-elements">
                <li class="list-group-item">
                  <a href="torta_psd_prototype/home_v1.php">Psd prototype template</a>
                </li>
                <li class="list-group-item">
                  <a href="torta_psd_prototype/home_v1.php">Psd lego template</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>

    </div>
    <!-- div.container -->

    <script charset="utf-8" src="assets/bower_components/jquery/dist/jquery.min.js" type="text/javascript"></script>
    <script charset="utf-8" src="assets/bower_components/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
  </body>
</html>
