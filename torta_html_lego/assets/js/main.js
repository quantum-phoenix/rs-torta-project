$(document).ready(function (e) {

    'use strict';
    /* -----------------------------------------------------
     Wow
     ----------------------------------------------------- */
    new WOW().init();
    /* -----------------------------------------------------
     Google map (Native)
     ----------------------------------------------------- */
    var gmDiv = document.getElementById('google-maps');
    if (gmDiv != null) {
        var styles = [
            {
                featureType: 'water',
                elementType: 'geometry.fill',
                stylers: [
                    {color: '#adc9b8'}
                ]
            }, {
                featureType: 'landscape.natural',
                elementType: 'all',
                stylers: [
                    {hue: '#809f80'},
                    {lightness: -35}
                ]
            }
            , {
                featureType: 'poi',
                elementType: 'geometry',
                stylers: [
                    {hue: '#f9e0b7'},
                    {lightness: 30}
                ]
            }, {
                featureType: 'road',
                elementType: 'geometry',
                stylers: [
                    {hue: '#d5c18c'},
                    {lightness: 14}
                ]
            }, {
                featureType: 'road.local',
                elementType: 'all',
                stylers: [
                    {hue: '#ffd7a6'},
                    {saturation: 100},
                    {lightness: -12}
                ]
            }
        ];
        var gmPosition = new google.maps.LatLng(51.508742, -0.120850);
        var gmOptions = {
            mapTypeControlOptions: {
                mapTypeIds: ['Styled']
            },
            center: gmPosition,
            zoom: 16,
            disableDefaultUI: true,
            mapTypeId: 'Styled'
        };
        var gmMap = new google.maps.Map(gmDiv, gmOptions);
        var marker = new google.maps.Marker({
            position: gmPosition,
            icon: 'assets/images/map_marker.png'
        });
        marker.setMap(gmMap);
        var styledMapType = new google.maps.StyledMapType(styles, {name: 'Styled'});
        gmMap.mapTypes.set('Styled', styledMapType);
    }
    /* -----------------------------------------------------
     Slick slider
     ----------------------------------------------------- */
    $('.slick-slider-single').slick({
        dots: false,
        arrows: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        prevArrow: '<button type="button" class="slick-prev slick-nav"><i class="fa fa-angle-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next slick-nav"><i class="fa fa-angle-right"></i></button>',
        cssEase: 'cubic-bezier(0.785, 0.135, 0.150, 0.860)'
    });
    /* Four column ***/
    $('.js-slick-slider-four-without-arrows').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        speed: 300,
        adaptiveHeight: true,
        autoplay: true,
        prevArrow: '<button type="button" class="slick-prev slick-nav"><i class="fa fa-angle-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next slick-nav"><i class="fa fa-angle-right"></i></button>',
        cssEase: 'cubic-bezier(0.785, 0.135, 0.150, 0.860)',
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    /* Four column ***/
    $('.js-slick-slider-four-with-arrows').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        speed: 300,
        adaptiveHeight: true,
        autoplay: true,
        prevArrow: '<button type="button" class="slick-prev slick-nav"><i class="fa fa-angle-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next slick-nav"><i class="fa fa-angle-right"></i></button>',
        cssEase: 'cubic-bezier(0.785, 0.135, 0.150, 0.860)',
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    /* -----------------------------------------------------
     Marsonry
     ----------------------------------------------------- */
    var $iso = $('.masonry-blog-content-type').isotope({
        layoutMode: 'packery',
        itemSelector: '.masonry-blog-element',
        percentPosition: true,
        packery: {
            columnWidth: 288
        }
    });
    $iso.imagesLoaded().progress(function () {
        $iso.isotope('layout');
    });
    /* -----------------------------------------------------
     Same side force
     ----------------------------------------------------- */
    $('.same-side-force').each(function () {
        var width = $(this).width();
        var height = $(this).height();
        if (width < height) {
            $(this).width(height);
        } else {
            $(this).height(width);
        }
    });
    /* -----------------------------------------------------
     Range slider
     ----------------------------------------------------- */
    $('#price-filter').each(function () {
        var priceFilterSlider = document.getElementById('price-filter');
        noUiSlider.create(priceFilterSlider, {
            start: [10, 35],
            connect: false,
            step: 1,
            range: {
                'min': 3,
                'max': 55
            }
        });
        var inputFormat = document.getElementById('price-filter-value');
        var low, high = 0;
        priceFilterSlider.noUiSlider.on('update', function (values, handle) {
            if (!handle) {
                low = values[handle];
            } else {
                high = values[handle];
            }
            inputFormat.value = "Price: " + "$" + low + " - " + "$" + high;
        });
        inputFormat.addEventListener('change', function () {
            priceFilterSlider.noUiSlider.set(this.value);
        });
    });
    /* -----------------------------------------------------
     Revolution slider
     ----------------------------------------------------- */
    $(document).ready(function () {
        $('.tp-banner').revolution(
            {
                delay: 9000,
                startwidth: 1170,
                startheight: 500,
                hideThumbs: 10
            });
    });
    /* -----------------------------------------------------
     Isotope
     ----------------------------------------------------- */
    var $container = $('.portfolio-content-type');

    var $port = $container.isotope({
        itemSelector: '.portfolio-element'
    });

    $port.imagesLoaded().progress(function () {
        $port.isotope('layout');
    });

    $('.isotope-filters button').click(function () {
        $('.portfolioFilter .is-checked').removeClass('is-checked');
        $(this).addClass('is-checked');

        var selector = $(this).attr('data-filter');
        $container.isotope({
            filter: selector,
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });
        return false;
    });

    /* -----------------------------------------------------
     Slickmenu
     ----------------------------------------------------- */
    $('#slick_menu').slicknav();
    /* -----------------------------------------------------
     Selectpicker
     ----------------------------------------------------- */
    $('.selectpicker').selectpicker();
    /* -----------------------------------------------------
     Raty
     ----------------------------------------------------- */
    $('div.raty-plugin').raty({
        path: 'assets/images',
        score: 3
    });
    /* -----------------------------------------------------
     Preloader
     ----------------------------------------------------- */
    var $preloader = $('.load-screen');
    $(window).load(function () {
        $preloader.delay(400).fadeOut(500);
    })


    /* -----------------------------------------------------
     Mini cart
     ----------------------------------------------------- */
    var $target;
    $(".mini-cart").on('mouseenter', function (event) {
        $target = $(event.currentTarget);
        $target.find(".mini-cart-dropdown").stop(true, true).slideDown()
    }).on('mouseleave', function (event) {
        $target = $(event.currentTarget);
        $target.find(".mini-cart-dropdown").stop(true, true).delay(1500).slideUp();
    });
    /* -----------------------------------------------------
     Back to top
     ----------------------------------------------------- */
    if ( ($(window).height() + 100) < $(document).height() ) {
        $('#back-to-top').removeClass('hidden').affix({
            // how far to scroll down before link "slides" into view
            offset: {top:300}
        });
    }
    /* -----------------------------------------------------
     Progressbar
     ----------------------------------------------------- */
    $('.progress').waypoint({
        handler: function(direction) {
            $('.progress .progress-bar').progressbar({display_text: 'fill'});
        },
        offset: '75%'
    })

});