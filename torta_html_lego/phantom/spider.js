console.log('*** Current PhantomJS version: ' +
            phantom.version.major + '.' +
            phantom.version.minor + '.' +
            phantom.version.patch);

var page = require('webpage').create(),
    system = require('system'),
    fs = require('fs');

page.settings.userAgent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36';

if (system.args.length === 1) {
  console.log('Try to pass some args when invoking this script!');
  phantom.exit();
}

//var url = 'http://localhost/dev/torta_project/torta_html_lego/template/sites/cart.php';
var url = system.args[1];
var filename = url.substring(url.lastIndexOf('/') + 1);

console.log('*** Filename: ' + filename);

page.open(url, function (status) {

  if (status !== 'success') {
    console.log('*** Unable to access network');
  }

});

page.onLoadFinished = function () {
  console.log("*** Page load finished!");
  //page.render("images/" + filename + '.png');
  fs.write("private/" + filename, page.content, 'w');
  phantom.exit();
};




