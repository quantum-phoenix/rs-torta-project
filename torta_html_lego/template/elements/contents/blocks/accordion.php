<div class="rs-accordion">
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Who are we?</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium exercitationem facere illo
                        incidunt, minima, necessitatibus nemo, nihil nulla provident quasi sit tempora unde? Accusantium
                        iusto minima mollitia numquam possimus quaerat quas quis rem! Consequuntur exercitationem id
                        minus saepe voluptate? Voluptatum!</p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Our mission?</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium exercitationem facere illo
                        incidunt, minima, necessitatibus nemo, nihil nulla provident quasi sit tempora unde? Accusantium
                        iusto minima mollitia numquam possimus quaerat quas quis rem! Consequuntur exercitationem id
                        minus saepe voluptate? Voluptatum!</p>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">What we do?</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium exercitationem facere illo
                        incidunt, minima, necessitatibus nemo, nihil nulla provident quasi sit tempora unde? Accusantium
                        iusto minima mollitia numquam possimus quaerat quas quis rem! Consequuntur exercitationem id
                        minus saepe voluptate? Voluptatum!</p>
                </div>
            </div>
        </div>
    </div>
</div>