<?php //http://bootsnipp.com/snippets/featured/comment-posts-layout ?>
<h3>Comments (4)</h3>
<section class="comment-list<?php echo $scroll; ?>">

    <!-- 1. comment -->
    <div class="row">
        <div class="col-md-2 col-sm-2 hidden-xs">
            <figure class="thumbnail">
                <img class="img-responsive" src="<?php echo $pictures['comment_picture_one']; ?>"/>
                <figcaption class="text-center">Monica Doe</figcaption>
            </figure>
        </div>
        <div class="col-md-10 col-sm-10">
            <div class="panel panel-default arrow left">
                <div class="panel-body">
                    <header class="text-left">
                        <div class="comment-date"><i class="fa fa-clock-o"></i> Dec 16,
                            2014
                        </div>
                    </header>
                    <div class="comment-post">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                    <p class="text-right"><a href="#" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i>
                            reply</a></p>
                </div>
            </div>
        </div>
    </div>
    <!-- 2. comment -->
    <div class="row">
        <div class="col-md-2 col-sm-2 col-md-offset-1 col-sm-offset-0 hidden-xs">
            <figure class="thumbnail">
                <img class="img-responsive" src="<?php echo $pictures['comment_picture_two']; ?>"/>
                <figcaption class="text-center">Adam Doe</figcaption>
            </figure>
        </div>
        <div class="col-md-9 col-sm-9">
            <div class="panel panel-default arrow left">
                <div class="panel-heading right">Reply</div>
                <div class="panel-body">
                    <header class="text-left">
                        <div class="comment-date"><i class="fa fa-clock-o"></i> Dec 16,
                            2014
                        </div>
                    </header>
                    <div class="comment-post">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                    <p class="text-right"><a href="#" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i>
                            reply</a></p>
                </div>
            </div>
        </div>
    </div>
    <!-- 3. comment -->
    <div class="row">
        <div class="col-md-2 col-sm-2 col-md-offset-2 col-sm-offset-0 hidden-xs">
            <figure class="thumbnail">
                <img class="img-responsive" src="<?php echo $pictures['comment_picture_three']; ?>"/>
                <figcaption class="text-center">Jessica Doe</figcaption>
            </figure>
        </div>
        <div class="col-md-8 col-sm-8">
            <div class="panel panel-default arrow left">
                <div class="panel-heading right">Reply</div>
                <div class="panel-body">
                    <header class="text-left">
                        <div class="comment-date"><i class="fa fa-clock-o"></i> Dec 16,
                            2014
                        </div>
                    </header>
                    <div class="comment-post">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                    <p class="text-right"><a href="#" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i>
                            reply</a></p>
                </div>
            </div>
        </div>
    </div>
    <!-- 4. comment -->
    <div class="row">
        <div class="col-md-2 col-sm-2 hidden-xs">
            <figure class="thumbnail">
                <img class="img-responsive" src="<?php echo $pictures['comment_picture_four']; ?>"/>
                <figcaption class="text-center">John Doe</figcaption>
            </figure>
        </div>
        <div class="col-md-10 col-sm-10">
            <div class="panel panel-default arrow left">
                <div class="panel-body">
                    <header class="text-left">
                        <div class="comment-date"><i class="fa fa-clock-o"></i> Dec 16,
                            2014
                        </div>
                    </header>
                    <div class="comment-post">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                            laboris nisi ut aliquip ex ea commodo consequat.
                        </p>
                    </div>
                    <p class="text-right"><a href="#" class="btn btn-primary btn-sm"><i class="fa fa-reply"></i>
                            reply</a></p>
                </div>
            </div>
        </div>
    </div>

    <h3>Leave a Reply</h3>

    <form>
        <div class="row row-col-half-margin">
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" id="username" class="form-control form-control-bg" name="username"
                           placeholder="Name">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" id="email" class="form-control form-control-bg" name="email" placeholder="Email">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" id="website" class="form-control form-control-bg" name="website"
                           placeholder="Website">
                </div>
            </div>
        </div>
        <div class="row row-col-half-margin">
            <div class="col-md-12">
                <div class="form-group">
                    <textarea class="form-control form-control-bg" rows="6" placeholder="Comment"></textarea>
                </div>
            </div>
        </div>
        <div class="row row-col-no-margin">
            <div class="col-md-12">
                <div class="form-group">
                    <input class="btn btn-primary" type="submit" value="Submit">
                </div>
            </div>
        </div>
    </form>

</section>