<div class="row<?php echo $scroll; ?>">
    <div class="col-md-12">
        <?php $heading = isset($heading) ? $heading : "Example Heading Decor Version 1"; ?>
        <div class="decor-heading-parent">
            <div class="decor-heading">
                <h3><?php echo $heading; ?></h3>
            </div>
        </div>
    </div>
</div>
