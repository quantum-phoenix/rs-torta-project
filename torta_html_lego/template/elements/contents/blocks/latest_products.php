<div class="row shop-content-type shop-content-type-latest-products">

    <div class="col-md-3">
        <article class="shop-element<?php echo $scroll; ?>">
            <div class="entry-image thumbnail-image">
                <div class="int-image-hover-parent">
                    <img class="responsive-image" src="<?php echo $pictures['releated_portfolio_one']; ?>"
                         alt="marsonry_blog_picture_one"/>

                    <div class="int-image-hover">
                        <a href="">
                            <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="entry-item-rating">
                <?php include "../elements/contents/blocks/item-rating.php"; ?>
            </div>
            <div class="entry-title entry-title-center"><h4>Lorem Ipsum Dolor Sit</h4></div>
            <div class="entry-shop-details">
                <div class="left"><a href="#">Add to cart</a></div>
                <div class="right"><p>$5.5</p></div>
            </div>
            <div class="entry-more-details">
                <a href="<?php echo !$html_or_php ? "single_product.php" : "single_product.html"; ?>">More Details</a>
            </div>
        </article>
    </div>

    <div class="col-md-3">
        <article class="shop-element<?php echo $scroll; ?>">
            <div class="entry-image thumbnail-image">
                <div class="int-image-hover-parent">
                    <img class="responsive-image" src="<?php echo $pictures['releated_portfolio_two']; ?>"
                         alt="marsonry_blog_picture_two"/>

                    <div class="int-image-hover">
                        <a href="<?php echo !$html_or_php ? "single_product.php" : "single_product.html"; ?>">
                            <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="entry-item-rating">
                <?php include "../elements/contents/blocks/item-rating.php"; ?>
            </div>
            <div class="entry-title entry-title-center"><h4>Lorem Ipsum Dolor Sit</h4></div>
            <div class="entry-shop-details">
                <div class="left"><a href="#">Add to cart</a></div>
                <div class="right"><p>$5.5</p></div>
            </div>
            <div class="entry-more-details">
                <a href="<?php echo !$html_or_php ? "single_product.php" : "single_product.html"; ?>">More Details</a>
            </div>
        </article>
    </div>

    <div class="col-md-3">
        <article class="shop-element<?php echo $scroll; ?>">
            <div class="entry-image thumbnail-image">
                <div class="int-image-hover-parent">
                    <img class="responsive-image" src="<?php echo $pictures['releated_portfolio_three']; ?>"
                         alt="marsonry_blog_picture_three"/>

                    <div class="int-image-hover">
                        <a href="<?php echo !$html_or_php ? "single_product.php" : "single_product.html"; ?>">
                            <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="entry-item-rating">
                <?php include "../elements/contents/blocks/item-rating.php"; ?>
            </div>
            <div class="entry-title entry-title-center"><h4>Lorem Ipsum Dolor Sit</h4></div>
            <div class="entry-shop-details">
                <div class="left"><a href="#">Add to cart</a></div>
                <div class="right"><p>$5.5</p></div>
            </div>
            <div class="entry-more-details">
                <a href="<?php echo !$html_or_php ? "single_product.php" : "single_product.html"; ?>">More Details</a>
            </div>
        </article>
    </div>

    <div class="col-md-3">
        <article class="shop-element <?php echo $scroll; ?>">
            <div class="entry-image thumbnail-image">
                <div class="int-image-hover-parent">
                    <img class="responsive-image" src="<?php echo $pictures['releated_portfolio_four']; ?>"
                         alt="marsonry_blog_picture_four"/>

                    <div class="int-image-hover">
                        <a href="<?php echo !$html_or_php ? "single_product.php" : "single_product.html"; ?>">
                            <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="entry-item-rating">
                <?php include "../elements/contents/blocks/item-rating.php"; ?>
            </div>
            <div class="entry-title entry-title-center"><h4>Lorem Ipsum Dolor Sit</h4></div>
            <div class="entry-shop-details">
                <div class="left"><a href="#">Add to cart</a></div>
                <div class="right"><p>$5.5</p></div>
            </div>
            <div class="entry-more-details">
                <a href="<?php echo !$html_or_php ? "single_product.php" : "single_product.html"; ?>">More Details</a>
            </div>
        </article>
    </div>

</div>
