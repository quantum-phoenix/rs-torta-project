<div class="mini-cart">
    <a href="single_product.php">
        <ul class="ul-li-inline ul-li-horizontal-spacing-xs">
            <li><i class="fa fa-shopping-cart"></i></li>
            <li><span>My cart</span></li>
            <li><span class="mini-cart-counter">3</span></li>
        </ul>
    </a>

    <div class="mini-cart-dropdown">
        <p>2 Items, $259.99</p>
        <table class="table table-bordered cart-table">
            <tbody>
            <tr>
                <td><a href="#"><i class="fa fa-trash-o"></i></a></td>
                <td>1 x</td>
                <td><img src="<?php echo $pictures['mini_cart_one']; ?>" alt="mini_cart_one"></td>
                <td>Lorem ipsum</td>
                <td>$99</td>
            </tr>
            <tr>
                <td><a href="#"><i class="fa fa-trash-o"></i></a></td>
                <td>1 x</td>
                <td><img src="<?php echo $pictures['mini_cart_two']; ?>" alt="mini_cart_two"></td>
                <td>Lorem ipsum</td>
                <td>$99</td>
            </tr>
            <tr>
                <td><a href="#"><i class="fa fa-trash-o"></i></a></td>
                <td>1 x</td>
                <td><img src="<?php echo $pictures['mini_cart_three']; ?>" alt="mini_cart_three"></td>
                <td>Lorem ipsum</td>
                <td>$99</td>
            </tr>
            </tbody>
        </table>
        <div class="align-box">
            <div class="align-left half-box">
                <a href="checkout.php" class="btn btn-primary">Checkout</a>
            </div>
            <div class="align-left half-box">
                <a href="cart.php" class="btn btn-primary">View Cart</a>
            </div>
        </div>
    </div>
</div>