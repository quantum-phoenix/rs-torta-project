<h4>Cake</h4>
<div class="progress">
    <div class="progress-bar progress-bar-rs" role="progressbar" data-transitiongoal="40"></div>
</div>
<h4>Muffin</h4>
<div class="progress">
    <div class="progress-bar progress-bar-rs" role="progressbar" data-transitiongoal="60"></div>
</div>
<h4>Sweet</h4>
<div class="progress">
    <div class="progress-bar progress-bar-rs" role="progressbar" data-transitiongoal="80"></div>
</div>
<h4>Salty</h4>
<div class="progress">
    <div class="progress-bar progress-bar-rs" role="progressbar" data-transitiongoal="100"></div>
</div>