<div class="row row-col-no-margin">
    <div class="col-md-12">
        <div class="css-releated-gallery js-slick-slider-four-with-arrows<?php echo $scroll; ?>">

            <article class="releated-portfolio-element">
                <div class="entry">
                    <div class="entry-image thumbnail-image">
                        <div class="int-image-hover-parent">
                            <img class="responsive-image" src="<?php echo $pictures['releated_portfolio_one']; ?>"
                                 alt="releated_portfolio_one"/>

                            <div class="int-image-hover">
                                <a href="<?php echo !$html_or_php ? "single_portfolio.php" : "single_portfolio.html"; ?>">
                                    <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="entry-title-center">
                        <h4>Lorem ipsum</h4>
                    </div>
                    <div class="entry-tags">
                        <ul class="releated-portfolio-tags ul-li-inline ul-li-horizontal-spacing-xs">
                            <li><span class="negative">Muffin</span></li>
                            <li><span class="negative">/</span></li>
                            <li><span class="negative">Sweet</span></li>
                        </ul>
                    </div>
                </div>
            </article>

            <article class="releated-portfolio-element">
                <div class="entry">
                    <div class="entry-image thumbnail-image">
                        <div class="int-image-hover-parent">
                            <img class="responsive-image" src="<?php echo $pictures['releated_portfolio_two']; ?>"
                                 alt="releated_portfolio_two"/>

                            <div class="int-image-hover">
                                <a href="<?php echo !$html_or_php ? "single_portfolio.php" : "single_portfolio.html"; ?>">
                                    <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="entry-title-center">
                        <h4>Lorem ipsum</h4>
                    </div>
                    <div class="entry-tags">
                        <ul class="releated-portfolio-tags ul-li-inline ul-li-horizontal-spacing-xs">
                            <li><span class="negative">Muffin</span></li>
                            <li><span class="negative">/</span></li>
                            <li><span class="negative">Sweet</span></li>
                        </ul>
                    </div>
                </div>
            </article>

            <article class="releated-portfolio-element">
                <div class="entry">
                    <div class="entry-image thumbnail-image">
                        <div class="int-image-hover-parent">
                            <img class="responsive-image" src="<?php echo $pictures['releated_portfolio_three']; ?>"
                                 alt="releated_portfolio_three"/>

                            <div class="int-image-hover">
                                <a href="<?php echo !$html_or_php ? "single_portfolio.php" : "single_portfolio.html"; ?>">
                                    <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="entry-title-center">
                        <h4>Lorem ipsum</h4>
                    </div>
                    <div class="entry-tags">
                        <ul class="releated-portfolio-tags ul-li-inline ul-li-horizontal-spacing-xs">
                            <li><span class="negative">Muffin</span></li>
                            <li><span class="negative">/</span></li>
                            <li><span class="negative">Sweet</span></li>
                        </ul>
                    </div>
                </div>
            </article>

            <article class="releated-portfolio-element">
                <div class="entry">
                    <div class="entry-image thumbnail-image">
                        <div class="int-image-hover-parent">
                            <img class="responsive-image" src="<?php echo $pictures['releated_portfolio_four']; ?>"
                                 alt="releated_portfolio_four"/>

                            <div class="int-image-hover">
                                <a href="<?php echo !$html_or_php ? "single_portfolio.php" : "single_portfolio.html"; ?>">
                                    <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="entry-title-center">
                        <h4>Lorem ipsum</h4>
                    </div>
                    <div class="entry-tags">
                        <ul class="releated-portfolio-tags ul-li-inline ul-li-horizontal-spacing-xs">
                            <li><span class="negative">Muffin</span></li>
                            <li><span class="negative">/</span></li>
                            <li><span class="negative">Sweet</span></li>
                        </ul>
                    </div>
                </div>
            </article>

            <article class="releated-portfolio-element">
                <div class="entry">
                    <div class="entry-image thumbnail-image">
                        <div class="int-image-hover-parent">
                            <img class="responsive-image" src="<?php echo $pictures['releated_portfolio_five']; ?>"
                                 alt="releated_portfolio_five"/>

                            <div class="int-image-hover">
                                <a href="<?php echo !$html_or_php ? "single_portfolio.php" : "single_portfolio.html"; ?>">
                                    <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="entry-title-center">
                        <h4>Lorem ipsum</h4>
                    </div>
                    <div class="entry-tags">
                        <ul class="releated-portfolio-tags ul-li-inline ul-li-horizontal-spacing-xs">
                            <li><span class="negative">Muffin</span></li>
                            <li><span class="negative">/</span></li>
                            <li><span class="negative">Sweet</span></li>
                        </ul>
                    </div>
                </div>
            </article>

            <article class="releated-portfolio-element">
                <div class="entry">
                    <div class="entry-image thumbnail-image">
                        <div class="int-image-hover-parent">
                            <img class="responsive-image" src="<?php echo $pictures['releated_portfolio_six']; ?>"
                                 alt="releated_portfolio_six"/>

                            <div class="int-image-hover">
                                <a href="<?php echo !$html_or_php ? "single_portfolio.php" : "single_portfolio.html"; ?>">
                                    <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="entry-title-center">
                        <h4>Lorem ipsum</h4>
                    </div>
                    <div class="entry-tags">
                        <ul class="releated-portfolio-tags ul-li-inline ul-li-horizontal-spacing-xs">
                            <li><span class="negative">Muffin</span></li>
                            <li><span class="negative">/</span></li>
                            <li><span class="negative">Sweet</span></li>
                        </ul>
                    </div>
                </div>
            </article>

            <article class="releated-portfolio-element">
                <div class="entry">
                    <div class="entry-image thumbnail-image">
                        <div class="int-image-hover-parent">
                            <img class="responsive-image" src="<?php echo $pictures['releated_portfolio_seven']; ?>"
                                 alt="releated_portfolio_seven"/>

                            <div class="int-image-hover">
                                <a href="<?php echo !$html_or_php ? "single_portfolio.php" : "single_portfolio.html"; ?>">
                                    <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="entry-title-center">
                        <h4>Lorem ipsum</h4>
                    </div>
                    <div class="entry-tags">
                        <ul class="releated-portfolio-tags ul-li-inline ul-li-horizontal-spacing-xs">
                            <li><span class="negative">Muffin</span></li>
                            <li><span class="negative">/</span></li>
                            <li><span class="negative">Sweet</span></li>
                        </ul>
                    </div>
                </div>
            </article>

            <article class="releated-portfolio-element">
                <div class="entry">
                    <div class="entry-image thumbnail-image">
                        <div class="int-image-hover-parent">
                            <img class="responsive-image" src="<?php echo $pictures['releated_portfolio_eight']; ?>"
                                 alt="releated_portfolio_eight"/>

                            <div class="int-image-hover">
                                <a href="<?php echo !$html_or_php ? "single_portfolio.php" : "single_portfolio.html"; ?>">
                                    <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="entry-title-center">
                        <h4>Lorem ipsum</h4>
                    </div>
                    <div class="entry-tags">
                        <ul class="releated-portfolio-tags ul-li-inline ul-li-horizontal-spacing-xs">
                            <li><span class="negative">Muffin</span></li>
                            <li><span class="negative">/</span></li>
                            <li><span class="negative">Sweet</span></li>
                        </ul>
                    </div>
                </div>
            </article>

        </div>
    </div>
</div>