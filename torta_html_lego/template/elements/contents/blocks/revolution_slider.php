<div class="tp-banner-container">
    <div class="tp-banner">
        <ul>
            <!-- SLIDE  -->
            <li data-transition="zoomein" data-slotamount="7" data-masterspeed="1500">
                <!-- MAIN IMAGE -->
                <img src="<?php echo !$html_or_php ? "../../assets/images/rs_pattern_1.jpg" : "assets/images/rs_pattern_1.jpg"; ?>" alt="pattern_1" data-bgrepeat="repeat"
                     data-bgfit="normal" data-bgposition="center center">
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <div class="tp-caption lft customout rs-parallaxlevel-0"
                     data-x="left"
                     data-y="center"
                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="700"
                     data-start="1550"
                     data-easing="Power3.easeInOut"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     style=""><img src="<?php echo !$html_or_php ? "../../assets/images/rs_dummy.png" : "assets/images/rs_dummy.png"; ?>" alt=""
                                              data-lazyload="<?php echo $pictures['rs_one']; ?>">
                </div>
                <!-- LAYER NR. 2 -->
                <div
                    class="tp-caption tp-big-font-bold tp-white-text skewfromleftshort fadeout tp-resizeme rs-parallaxlevel-10"
                    data-x="right" data-hoffset="0"
                    data-y="center" data-voffset="-30"
                    data-speed="500"
                    data-start="2400"
                    data-easing="Power3.easeInOut"
                    data-splitin="chars"
                    data-splitout="none"
                    data-elementdelay="0.1"
                    data-endelementdelay="0.1"
                    data-endspeed="300"
                    style="">We develop incredibly
                </div>
                <!-- LAYER NR. 3 -->
                <div
                    class="tp-caption tp-big-font-normal tp-white-text skewfromleftshort fadeout tp-resizeme rs-parallaxlevel-10"
                    data-x="right" data-hoffset="0"
                    data-y="center" data-voffset="30"
                    data-speed="500"
                    data-start="3400"
                    data-easing="Power3.easeInOut"
                    data-splitin="chars"
                    data-splitout="none"
                    data-elementdelay="0.1"
                    data-endelementdelay="0.1"
                    data-endspeed="300"
                    style="">Innovative Designs
                </div>
                <!-- LAYER NR. 4 -->
                <div class="tp-caption customin tp-resizeme rs-parallaxlevel-0"
                     data-x="right"
                     data-y="center" data-voffset="95"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="2900"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-linktoslide="next"
                     style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;"><a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>" class="btn btn-primary btn-lg">Shop now</a>
                </div>


            </li>
            <!-- SLIDE  -->
            <li data-transition="zoomein" data-slotamount="7" data-masterspeed="1500">
                <!-- MAIN IMAGE -->
                <img src="<?php echo $pictures['rs_two']; ?>" alt="revolution_slider_two" data-bgrepeat="repeat"
                     data-bgfit="normal" data-bgposition="center center">
                <!-- LAYERS -->
                <!-- LAYER NR. 1 -->
                <div
                    class="tp-caption tp-big-font-bold tp-white-text skewfromleftshort fadeout tp-resizeme rs-parallaxlevel-10"
                    data-x="center" data-hoffset="0"
                    data-y="center" data-voffset="-30"
                    data-speed="500"
                    data-start="2400"
                    data-easing="Power3.easeInOut"
                    data-splitin="chars"
                    data-splitout="none"
                    data-elementdelay="0.1"
                    data-endelementdelay="0.1"
                    data-endspeed="300"
                    style="">Simply delicious
                </div>
                <!-- LAYER NR. 2 -->
                <div
                    class="tp-caption tp-big-font-normal tp-white-text skewfromleftshort fadeout tp-resizeme rs-parallaxlevel-10"
                    data-x="center" data-hoffset="0"
                    data-y="center" data-voffset="30"
                    data-speed="500"
                    data-start="3400"
                    data-easing="Power3.easeInOut"
                    data-splitin="chars"
                    data-splitout="none"
                    data-elementdelay="0.1"
                    data-endelementdelay="0.1"
                    data-endspeed="300"
                    style="">Little things make a big difference
                </div>
                <!-- LAYER NR. 3 -->
                <div class="tp-caption customin tp-resizeme rs-parallaxlevel-0"
                     data-x="center"
                     data-y="center" data-voffset="95"
                     data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                     data-speed="500"
                     data-start="2900"
                     data-easing="Power3.easeInOut"
                     data-splitin="none"
                     data-splitout="none"
                     data-elementdelay="0.1"
                     data-endelementdelay="0.1"
                     data-linktoslide="next"
                     style="z-index: 12; max-width: auto; max-height: auto; white-space: nowrap;"><a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>" class="btn btn-primary btn-lg">Shop now</a>
                </div>
            </li>
        </ul>
    </div>
    <div class="slider-decor-line-one"></div>
</div>