<div class="row services-block<?php echo $scroll; ?>">
    <div class="col-md-4">
        <div class="row row-col-no-margin">
            <div class="col-md-5">
                <img class="responsive-image services-img" src="<?php echo !$html_or_php ? "../../assets/svg/Roundicons-08.svg" : "assets/svg/Roundicons-08.svg"; ?>" alt="Roundicons">
            </div>
            <div class="col-md-7">
                <h3>Cake Design</h3>

                <p>Cu ignota forensibus inciderint eum. Nostrum appetere sea ea. Ea cum laoreet prodesset.</p>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="row row-col-no-margin">
            <div class="col-md-5">
                <img class="responsive-image services-img" src="<?php echo !$html_or_php ? "../../assets/svg/Roundicons-22.svg" : "assets/svg/Roundicons-22.svg"; ?>" alt="Roundicons">
            </div>
            <div class="col-md-7">
                <h3>New recipes</h3>

                <p>Cu ignota forensibus inciderint eum. Nostrum appetere sea ea. Ea cum laoreet prodesset.</p>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="row row-col-no-margin">
            <div class="col-md-5">
                <img class="responsive-image services-img" src="<?php echo !$html_or_php ? "../../assets/svg/Roundicons-43.svg" : "assets/svg/Roundicons-43.svg"; ?>" alt="Roundicons">
            </div>
            <div class="col-md-7">
                <h3>Desserts</h3>

                <p>Cu ignota forensibus inciderint eum. Nostrum appetere sea ea. Ea cum laoreet prodesset.</p>
            </div>
        </div>
    </div>
</div>

<div class="row services-block<?php echo $scroll; ?>">
    <div class="col-md-4">
        <div class="row row-col-no-margin">
            <div class="col-md-5">
                <img class="responsive-image services-img" src="<?php echo !$html_or_php ? "../../assets/svg/Roundicons-30.svg" : "assets/svg/Roundicons-30.svg"; ?>" alt="Roundicons">
            </div>
            <div class="col-md-7">
                <h3>Cake Design</h3>

                <p>Cu ignota forensibus inciderint eum. Nostrum appetere sea ea. Ea cum laoreet prodesset.</p>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="row row-col-no-margin">
            <div class="col-md-5">
                <img class="responsive-image services-img" src="<?php echo !$html_or_php ? "../../assets/svg/Roundicons-51.svg" : "assets/svg/Roundicons-51.svg"; ?>" alt="Roundicons">
            </div>
            <div class="col-md-7">
                <h3>New recipes</h3>

                <p>Cu ignota forensibus inciderint eum. Nostrum appetere sea ea. Ea cum laoreet prodesset.</p>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="row row-col-no-margin">
            <div class="col-md-5">
                <img class="responsive-image services-img" src="<?php echo !$html_or_php ? "../../assets/svg/Roundicons-56.svg" : "assets/svg/Roundicons-56.svg"; ?>" alt="Roundicons">
            </div>
            <div class="col-md-7">
                <h3>Desserts</h3>

                <p>Cu ignota forensibus inciderint eum. Nostrum appetere sea ea. Ea cum laoreet prodesset.</p>
            </div>
        </div>
    </div>
</div>