<div class="row">
    <div class="col-md-3">
        <div class="team-member<?php echo $scroll; ?>">
            <div class="team-thumb">
                <a href="#"><img src="<?php echo $pictures['avatar_picture_one']; ?>"
                                 class="responsive-image"
                                 alt="monica_doe"/></a>
            </div>
            <div class="team-content">
                <h3>Monica Doe</h3>

                <p>Chef</p>

                <p class="team-excerpt">Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming
                    id quod mazim placerat facer possim assum.</p>

                <div class="team-social">
                    <a href="#" class="btn btn-social-icon btn-facebook">
                        <i class="fa fa-facebook fa-lg"></i>
                    </a>
                    <a href="#" class="btn btn-social-icon btn-twitter">
                        <i class="fa fa-twitter fa-lg"></i>
                    </a>
                    <a href="#" class="btn btn-social-icon btn-vimeo">
                        <i class="fa fa-vimeo-square fa-lg"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="team-member<?php echo $scroll; ?>">
            <div class="team-thumb">
                <a href="#"><img src="<?php echo $pictures['avatar_picture_two']; ?>"
                                 class="responsive-image"
                                 alt="adam_doe"/></a>
            </div>
            <div class="team-content">
                <h3>Adam Doe</h3>

                <p>Chef</p>

                <p class="team-excerpt">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie
                    consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio
                    dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla
                    facilisi</p>

                <div class="team-social">
                    <a href="#" class="btn btn-social-icon btn-facebook">
                        <i class="fa fa-facebook fa-lg"></i>
                    </a>
                    <a href="#" class="btn btn-social-icon btn-twitter">
                        <i class="fa fa-twitter fa-lg"></i>
                    </a>
                    <a href="#" class="btn btn-social-icon btn-vimeo">
                        <i class="fa fa-vimeo-square fa-lg"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="team-member<?php echo $scroll; ?>">
            <div class="team-thumb">
                <a href="#"><img src="<?php echo $pictures['avatar_picture_three']; ?>"
                                 class="responsive-image"
                                 alt="john_doe"/></a>
            </div>
            <div class="team-content">
                <h3>John Doe</h3>

                <p>Chef</p>

                <p class="team-excerpt">Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper
                    suscipit
                    lobortis nisl ut aliquip ex ea commodo consequat.</p>

                <div class="team-social">
                    <a href="#" class="btn btn-social-icon btn-facebook">
                        <i class="fa fa-facebook fa-lg"></i>
                    </a>
                    <a href="#" class="btn btn-social-icon btn-twitter">
                        <i class="fa fa-twitter fa-lg"></i>
                    </a>
                    <a href="#" class="btn btn-social-icon btn-vimeo">
                        <i class="fa fa-vimeo-square fa-lg"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="team-member<?php echo $scroll; ?>">
            <div class="team-thumb">
                <a href="#"><img src="<?php echo $pictures['avatar_picture_four']; ?>"
                                 class="responsive-image"
                                 alt="jessica_doe"/></a>
            </div>
            <div class="team-content">
                <h3>Jessica Doe</h3>

                <p>Chef</p>

                <p class="team-excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy
                    nibh
                    euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>

                <div class="team-social">
                    <a href="#" class="btn btn-social-icon btn-facebook">
                        <i class="fa fa-facebook fa-lg"></i>
                    </a>
                    <a href="#" class="btn btn-social-icon btn-twitter">
                        <i class="fa fa-twitter fa-lg"></i>
                    </a>
                    <a href="#" class="btn btn-social-icon btn-vimeo">
                        <i class="fa fa-vimeo-square fa-lg"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>