<div class="testimonials"
     style="background:url(<?php echo $pictures['testimonials']; ?>);">

    <div class="slick-slider-single">
        <div class="slick-slider-item">
            <div class="testimonials-group">
                <div class="testimonials-avatar">
                    <img src="<?php echo $pictures['comment_picture_one']; ?>" alt="comment_picture_one">
                </div>
                <div class="testimonials-text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci alias cumque debitis dolorem
                        dolorum eius
                        error, ex exercitationem expedita illum impedit inventore ipsam ipsum itaque magnam minima
                        molestias natus
                        nobis officia perspiciatis quidem repudiandae saepe sed sequi totam velit, voluptas.</p>

                    <div class="divider-vertical postive-divider"></div>
                    <h3>JOHN LEE</h3>
                    <h5>Chef</h5>
                </div>
            </div>
        </div>
        <div class="slick-slider-item">
            <div class="testimonials-group">
                <div class="testimonials-avatar">
                    <img src="<?php echo $pictures['comment_picture_two']; ?>" alt="comment_picture_two">
                </div>
                <div class="testimonials-text">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci alias cumque debitis dolorem
                        dolorum eius
                        error, ex exercitationem expedita illum impedit inventore ipsam ipsum itaque magnam minima
                        molestias natus
                        nobis officia perspiciatis quidem repudiandae saepe sed sequi totam velit, voluptas.</p>

                    <div class="divider-vertical postive-divider"></div>
                    <h3>JOHN LEE</h3>
                    <h5>Chef</h5>
                </div>
            </div>
        </div>
    </div>
</div>