<div class="widget comments-list-widget<?php echo $scroll; ?>">
    <h3>Recent comments</h3>
    <ul class="comments-list">
        <li>Admin on <a href="#">Micro corrugated cardboard</a></li>
        <li>John Doe on <a href="#">Page with comments</a></li>
        <li>Monica Doe on <a href="#">Page with comments</a></li>
        <li>Jessica Doe on <a href="#">Page with comments</a></li>
    </ul>
</div>