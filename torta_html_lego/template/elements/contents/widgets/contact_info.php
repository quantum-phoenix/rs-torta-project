<div class="widget text-widget<?php echo $scroll; ?>">
    <h3>Contact info</h3>
    <ul>
        <li>
            <ul class="ul-li-inline ul-li-horizontal-padding-xs">
                <li class="bold"><i class="fa fa-phone"></i></li>
                <li><span class="negative">1800-222-222</span></li>
            </ul>
        </li>
        <li>
            <ul class="ul-li-inline ul-li-horizontal-padding-xs">
                <li class="bold"><i class="fa fa-envelope-o"></i></li>
                <li><span class="negative">contact@rubidiumstyle.com</span></li>
            </ul>
        </li>
        <li>
            <ul class="ul-li-inline ul-li-horizontal-padding-xs">
                <li class="bold"><i class="fa fa-clock-o"></i></li>
                <li><span class="negative">Everyday 9:00-17:00</span></li>
            </ul>
        </li>
        <li>
            <ul class="ul-li-inline ul-li-horizontal-spacing-xs">
                <li>Follow us:</li>
                <li>
                    <div class="standard-social">
                        <a href="#" class="btn rs-btn-social">
                            <i class="fa fa-facebook fa-lg"></i>
                        </a>
                        <a href="#" class="btn rs-btn-social">
                            <i class="fa fa-twitter fa-lg"></i>
                        </a>
                        <a href="#" class="btn rs-btn-social">
                            <i class="fa fa-vimeo-square fa-lg"></i>
                        </a>
                        <a href="#" class="btn rs-btn-social">
                            <i class="fa fa-youtube-play fa-lg"></i>
                        </a>
                    </div>
                </li>
            </ul>
        </li>
    </ul>
</div>