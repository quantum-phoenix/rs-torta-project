<div class="widget latest-products-widget<?php echo $scroll; ?>">
    <h3>Latest products</h3>
    <ul class="products">
        <li>
            <div class="split-box">
                <div class="left">
                    <a href="#"><img src="<?php echo $pictures['marsonry_blog_picture_one']; ?>"
                                     alt="marsonry_blog_picture_one"></a>
                </div>
                <div class="right">
                    <p class="bold">Lorem ipsum</p>

                    <p>Price: $5</p>
                </div>
            </div>
        </li>
        <li>
            <div class="split-box">
                <div class="left">
                    <a href="#"><img src="<?php echo $pictures['marsonry_blog_picture_two']; ?>"
                                     alt="marsonry_blog_picture_two"></a>
                </div>
                <div class="right">
                    <p class="bold">Lorem ipsum</p>

                    <p>Price: $5</p>
                </div>
            </div>
        </li>
        <li>
            <div class="split-box">
                <div class="left">
                    <a href="#"><img src="<?php echo $pictures['marsonry_blog_picture_three']; ?>"
                                     alt="marsonry_blog_picture_three"></a>
                </div>
                <div class="right">
                    <p class="bold">Lorem ipsum</p>

                    <p>Price: $5</p>
                </div>
            </div>
        </li>
    </ul>
</div>