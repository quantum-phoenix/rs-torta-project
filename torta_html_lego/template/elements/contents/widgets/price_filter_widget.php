<div class="widget price-filter-widget<?php echo $scroll; ?>">
    <h3>Filter by price</h3>

    <div id="price-filter" class=""></div>
    <div class="filter-set">
        <div class="left">
            <span class="input-group-btn">
                <button class="btn btn-primary" type="button">Filter</button>
            </span>
        </div>
        <div class="right">
            <input type="text" id="price-filter-value" readonly>
        </div>
    </div>
</div>