<div class="widget recent-posts-widget<?php echo $scroll; ?>">
    <h3>Recent Posts</h3>
    <ul class="recent-posts">
        <li><a href="#">Standard blog post</a></li>
        <li><a href="#">Slider blog post</a></li>
        <li><a href="#">Video blog post</a></li>
    </ul>
</div>
