<div class="widget tag-cloud-widget<?php echo $scroll; ?>">
    <h3>Tags</h3>

    <div class="tag-cloud">
        <a href="#">Sweet</a>
        <a href="#">Cake</a>
        <a href="#">Muffin</a>
        <a href="#">Meringue</a>
        <a href="#">Salty</a>
        <a href="#">Diabetic</a>
        <a href="#">Blog</a>
        <a href="#">Portfolio</a>
        <a href="#">News</a>
    </div>
</div>