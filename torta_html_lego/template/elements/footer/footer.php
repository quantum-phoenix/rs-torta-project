<footer>
    <?php include "primary_footer.php"; ?>
    <?php include "secondary_footer.php"; ?>
</footer>
<span id="back-to-top" class="hidden">
    <a href="#top" class=""  onclick="$('html,body').animate({scrollTop:0},'slow');return false;">
        <i class="glyphicon glyphicon-chevron-up"></i>
    </a>
</span>
</div>