<div class="footer-decor-line-one"></div>
<div class="primary-footer">

    <div class="container">
        <?php
        $url = basename($_SERVER['PHP_SELF']);
        if ($url == "home.php") {
            include "theme_footer_12_col.php";
        } else {
            include "theme_footer_3_col.php";
        }
        ?>
    </div>
</div>