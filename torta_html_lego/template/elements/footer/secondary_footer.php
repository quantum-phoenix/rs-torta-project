<div class="secondary-footer">
    <div class="container">

        <div class="row row-col-no-margin">
            <div class="col-md-6">
                <nav class="footer-menu">
                    <ul class="">
                        <li><a href="#">Home</a></li>
                        <li>/</li>
                        <li><a href="#">Blog</a></li>
                        <li>/</li>
                        <li><a href="#">Store</a></li>
                        <li>/</li>
                        <li><a href="#">Portfolio</a></li>
                        <li>/</li>
                        <li><a href="#">About us</a></li>
                        <li>/</li>
                        <li><a href="#">Contact</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-6">
                <p>Powered By WordPress , Copyright 2015 All Right Reserved</p>
            </div>
        </div>

    </div>
</div>