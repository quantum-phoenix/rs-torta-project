<div class="row row-col-no-margin<?php echo $scroll; ?>">
    <div class="col-md-12">
        <h3>Call us today.: 555.777.1112</h3>

        <div class="footer-social">
            <a href="#" class="btn rs-btn-social">
                <i class="fa fa-facebook fa-lg"></i>
            </a>
            <a href="#" class="btn rs-btn-social">
                <i class="fa fa-twitter fa-lg"></i>
            </a>
            <a href="#" class="btn rs-btn-social">
                <i class="fa fa-vimeo-square fa-lg"></i>
            </a>
            <a href="#" class="btn rs-btn-social">
                <i class="fa fa-youtube-play fa-lg"></i>
            </a>
        </div>
        <h3>Address: 123 Main Street, Anytown,CA 12345 </h3>
    </div>
</div>