<div class="row row-col-no-margin">

    <div class="col-md-3">
        <?php include '../elements/contents/widgets/text_widget.php'; ?>
    </div>

    <div class="col-md-3">
        <?php include '../elements/contents/widgets/categories_widget.php'; ?>
    </div>

    <div class="col-md-3">
        <?php include '../elements/contents/widgets/recent_posts_widget.php'; ?>
    </div>

    <div class="col-md-3">
        <?php include '../elements/contents/widgets/contact_info.php'; ?>
    </div>

</div>