<div class="load-screen">
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div>
</div>
<div class="wrapper">
    <header>
        <?php include "primary_head.php"; ?>
        <?php include "secondary_head.php"; ?>
        <?php include "tertiary_head.php"; ?>
        <?php include "quaternary_head.php"; ?>
    </header>