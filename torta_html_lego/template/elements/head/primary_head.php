<div class="primary-head">

    <div class="container">
        <div class="row row-col-no-margin">
            <div class="col-md-4">
                <h4 class="site-description">We are TORTA - A responsive HTML5 theme</h4>
            </div>
            <div class="col-md-4">
                <div class="primary-head-circle-parent">
                    <div class="primary-head-circle"></div>
                    <div class="logo">
                        <img src="<?php echo $pictures['logo']; ?>" alt="logo">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <?php include "../elements/contents/blocks/mini_cart.php"; ?>
            </div>
        </div>
    </div>

</div>