<div class="quaternary-head">
    <ul id="slick_menu">
        <li><a href="../sites/home.php">Home</a></li>
        <li><a href="../sites/list_blog_with_sidebar.php">List Blog With Sidebar</a></li>
        <li><a href="../sites/list_blog_without_sidebar.php">List Blog without Sidebar</a></li>
        <li><a href="../sites/masonry_blog.php">Marsonry Blog</a></li>
        <li><a href="../sites/single_post.php">Single Post</a></li>
        <li><a href="../sites/shop_with_sidebar.php">Store With Sidebar</a></li>
        <li><a href="../sites/shop_without_sidebar.php">Store Without Sidebar</a></li>
        <li><a href="../sites/single_product.php">Single Product</a></li>
        <li><a href="../sites/checkout.php">Checkout</a></li>
        <li><a href="../sites/cart.php">Cart</a></li>
        <li><a href="../sites/gallery.php">Gallery</a></li>
        <li><a href="../sites/single_gallery.php">Single Gallery</a></li>
        <li><a href="../sites/about_us.php">About us</a></li>
        <li><a href="../sites/contact.php">Contact</a></li>
    </ul>
</div>