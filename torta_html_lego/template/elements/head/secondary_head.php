<div class="secondary-head">

    <div class="container main-menu-container">

        <div class="row row-col-no-margin">
            <div class="col-md-6">
                <nav class="left-main-menu">
                    <ul class="ul-li-inline">
                        <li><a href="<?php echo !$html_or_php ? "../sites/home.php" : "home.html"; ?>"><h3>Home</h3></a></li>
                        <li>
                            <a href="<?php echo !$html_or_php ? "../sites/list_blog_with_sidebar.php" : "list_blog_with_sidebar.html"; ?>"><h3>Blog</h3></a>
                            <ul>
                                <li><a href="<?php echo !$html_or_php ? "../sites/list_blog_with_sidebar.php" : "list_blog_with_sidebar.html"; ?>">List Blog With Sidebar</a></li>
                                <li><a href="<?php echo !$html_or_php ? "../sites/list_blog_without_sidebar.php" : "list_blog_without_sidebar.html"; ?>">List Blog without Sidebar</a></li>
                                <li><a href="<?php echo !$html_or_php ? "../sites/masonry_blog.php" : "masonry_blog.html"; ?>">Marsonry Blog</a></li>
                                <li><a href="<?php echo !$html_or_php ? "../sites/single_post.php" : "single_post.html"; ?>">Single Post</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo !$html_or_php ? "../sites/shop_with_sidebar.php" : "shop_with_sidebar.html"; ?>"><h3>Store</h3></a>
                            <ul>
                                <li><a href="<?php echo !$html_or_php ? "../sites/shop_with_sidebar.php" : "shop_with_sidebar.html"; ?>">Store With Sidebar</a></li>
                                <li><a href="<?php echo !$html_or_php ? "../sites/shop_without_sidebar.php" : "shop_without_sidebar.html"; ?>">Store Without Sidebar</a></li>
                                <li><a href="<?php echo !$html_or_php ? "../sites/single_product.php" : "single_product.html"; ?>">Single Product</a></li>
                                <li><a href="<?php echo !$html_or_php ? "../sites/checkout.php" : "checkout.html"; ?>">Checkout</a></li>
                                <li><a href="<?php echo !$html_or_php ? "../sites/cart.php" : "cart.html"; ?>">Cart</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="col-md-6">
                <nav class="right-main-menu">
                    <ul class="ul-li-inline">
                        <li><a href="<?php echo !$html_or_php ? "../sites/portfolio.php" : "portfolio.html"; ?>"><h3>Portfolio</h3></a>
                            <ul>
                                <li><a href="<?php echo !$html_or_php ? "../sites/single_portfolio.php" : "single_portfolio.html"; ?>">Single Portfolio</a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo !$html_or_php ? "../sites/about_us.php" : "about_us.html"; ?>"><h3>About us</h3></a></li>
                        <li><a href="<?php echo !$html_or_php ? "../sites/contact.php" : "contact.html"; ?>"><h3>Contact</h3></a></li>
                    </ul>
                </nav>
            </div>
        </div>

    </div>

</div>