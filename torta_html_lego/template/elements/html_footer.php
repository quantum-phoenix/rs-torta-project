</section>
<?php include "../elements/footer/footer.php"; ?>
<!-- JS Global -->
<?php if (!$html_or_php): // *************************** ?>
    <script type="text/javascript" charset="utf-8" src="../../assets/bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../assets/bower_components/slick.js/slick/slick.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../assets/bower_components/isotope/dist/isotope.pkgd.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../assets/bower_components/isotope-packery/packery-mode.pkgd.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../assets/bower_components/imagesloaded/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../assets/offline_components/nouislider/nouislider.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../assets/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../assets/offline_components/raty_master/jquery.raty.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../assets/offline_components/slicknav/jquery.slicknav.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../assets/offline_components/rs_plugin/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../assets/offline_components/rs_plugin/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../assets/bower_components/wow.js/dist/wow.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../assets/bower_components/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../assets/bower_components/waypoints/lib/jquery.waypoints.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="http://maps.googleapis.com/maps/api/js"></script>
    <script type="text/javascript" charset="utf-8" src="../../assets/js/main.js"></script>
<?php else: // *************************** ?>
    <script type="text/javascript" charset="utf-8" src="assets/js/jquery.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="assets/js/slick.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="assets/js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="assets/js/packery-mode.pkgd.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="assets/js/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="assets/js/nouislider.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="assets/js/bootstrap-select.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="assets/raty_plugin/jquery.raty.js"></script>
    <script type="text/javascript" charset="utf-8" src="assets/js/jquery.slicknav.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="assets/rs_plugin/js/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="assets/rs_plugin/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="assets/js/wow.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="assets/js/bootstrap-progressbar.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="assets/js/jquery.waypoints.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="http://maps.googleapis.com/maps/api/js"></script>
    <script type="text/javascript" charset="utf-8" src="assets/js/main.js"></script>
<?php endif; // *************************** ?>

</body>
</html>