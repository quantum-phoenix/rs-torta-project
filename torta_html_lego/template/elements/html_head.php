<?php $html_or_php = true // false = php ?>
    <!DOCTYPE html>
    <!--[if lt IE 7 ]>
    <html class="ie ie6" lang="en"><![endif]-->
    <!--[if IE 7 ]>
    <html class="ie ie7" lang="en"><![endif]-->
    <!--[if IE 8 ]>
    <html class="ie ie8" lang="en"><![endif]-->
    <html lang="en">
    <head>
        <meta charset="UTF-8">

        <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Torta lego</title>

        <!-- Favicons -->
        <link rel="apple-touch-icon" sizes="57x57" href="assets/favicons/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="assets/favicons/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="assets/favicons/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="assets/favicons/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="assets/favicons/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="assets/favicons/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="assets/favicons/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="assets/favicons/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="assets/favicons/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="assets/favicons/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="assets/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="assets/favicons/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="assets/favicons/favicon-16x16.png">
        <link rel="manifest" href="assets/favicons/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="assets/favicons/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <!-- CSS Global -->
        <?php if (!$html_or_php): // *************************** ?>
            <link rel="stylesheet" type="text/css"
                  href="../../assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
            <link rel="stylesheet" type="text/css"
                  href="../../assets/bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
            <link rel="stylesheet" type="text/css"
                  href="../../assets/bower_components/font-awesome/css/font-awesome.min.css">

            <link rel="stylesheet" type="text/css" href="../../assets/bower_components/slick.js/slick/slick.css">
            <link rel="stylesheet" type="text/css" href="../../assets/bower_components/slick.js/slick/slick-theme.css">

            <link rel="stylesheet" type="text/css" href="../../assets/offline_components/nouislider/nouislider.min.css">

            <link rel="stylesheet" type="text/css" href="../../assets/offline_components/rs_plugin/css/settings.css">

            <link rel="stylesheet" type="text/css"
                  href="../../assets/offline_components/rubidium_social/less/rubidium-social.css">

            <link rel="stylesheet" type="text/css" href="../../assets/offline_components/slicknav/slicknav.min.css">

            <link rel="stylesheet" type="text/css"
                  href="../../assets/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css">
            <link rel="stylesheet" type="text/css" href="../../assets/offline_components/raty_master/jquery.raty.css">

            <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300' rel='stylesheet'
                  type='text/css'>
            <link href='http://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>

            <link rel="stylesheet" type="text/css" href="../../assets/bower_components/wow.js/css/libs/animate.css">
            <link rel="stylesheet" type="text/css"
                  href="../../assets/bower_components/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css">
            <link rel="stylesheet" type="text/css" href="../../assets/css/theme.css">

            <!--[if lt IE 9]>
            <script type="text/javascript" charset="utf-8"
                    src="../../assets/bower_components/respond/dest/respond.min.js"></script>
            <script type="text/javascript" charset="utf-8"
                    src="../../assets/bower_components/html5shiv/dist/html5shiv.min.js"></script>
            <![endif]-->
        <?php else: // *************************** ?>
            <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
            <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-theme.min.css">
            <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
            <link rel="stylesheet" type="text/css" href="assets/slick_plugin/slick.css">
            <link rel="stylesheet" type="text/css" href="assets/slick_plugin/slick-theme.css">
            <link rel="stylesheet" type="text/css" href="assets/css/nouislider.min.css">
            <link rel="stylesheet" type="text/css" href="assets/rs_plugin/css/settings.css">
            <link rel="stylesheet" type="text/css" href="assets/css/rubidium-social.css">
            <link rel="stylesheet" type="text/css" href="assets/css/slicknav.min.css">
            <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-select.min.css">
            <link rel="stylesheet" type="text/css" href="assets/raty_plugin/jquery.raty.css">
            <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300' rel='stylesheet' type='text/css'>
            <link href='http://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
            <link rel="stylesheet" type="text/css" href="assets/css/animate.css">
            <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-progressbar-3.3.4.min.css">
            <link rel="stylesheet" type="text/css" href="assets/css/theme.css">
            <!--[if lt IE 9]>
            <script type="text/javascript" charset="utf-8" src="assets/js/respond.min.js"></script>
            <script type="text/javascript" charset="utf-8" src="assets/js/html5shiv.min.js"></script>
            <![endif]-->
        <?php endif; // *************************** ?>
        <!--[if lt IE 7]>
        <style type="text/css">
            #wrapper {
                height: 100%;
            }
        </style>
        <![endif]-->
    </head>
<body>
<?php
if($html_or_php){
    $pictures = array(
        "logo" => "../../assets/images/logo.png", //118*187
        "avatar_picture_one" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/avatar_picture_one.jpg", //360*360
        "avatar_picture_two" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/avatar_picture_two.jpg", //360*360
        "avatar_picture_three" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/avatar_picture_three.jpg", //360*360
        "avatar_picture_four" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/avatar_picture_four.jpg", //360*36
        "rs_one" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/rs_one.png", //588*588
        "rs_two" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/rs_two.jpg", //588*588
        "about_us_one" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/about_us_one.png", //588*588
        "blog_picture_one" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/blog_picture_one.jpg", //1152*455
        "blog_picture_two" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/blog_picture_two.jpg", //1152*455
        "blog_picture_three" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/blog_picture_three.jpg", //1152*455
        "blog_picture_four" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/blog_picture_four.jpg", //1152*455
        "marsonry_blog_picture_one" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/marsonry_blog_picture_one.jpg", //264*264
        "marsonry_blog_picture_two" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/marsonry_blog_picture_two.jpg", //264*264
        "marsonry_blog_picture_three" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/marsonry_blog_picture_three.jpg", //264*264
        "marsonry_blog_picture_four" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/marsonry_blog_picture_four.jpg", //264*264
        "marsonry_blog_picture_five" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/marsonry_blog_picture_five.jpg", //264*264
        "marsonry_blog_picture_six" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/marsonry_blog_picture_six.jpg", //264*264
        "marsonry_blog_picture_seven" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/marsonry_blog_picture_seven.jpg", //264*264
        "marsonry_blog_picture_eight" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/marsonry_blog_picture_eight.jpg", //264*264
        "marsonry_blog_picture_nine" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/marsonry_blog_picture_nine.jpg", //264*264
        "marsonry_blog_picture_ten" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/marsonry_blog_picture_ten.jpg", //264*264
        "testimonials" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/testimonials.jpg", //1680*505
        "single_product_one" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/single_product_one.jpg", //466*466
        "portfolio_one" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/portfolio_one.jpg", //384*260
        "portfolio_two" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/portfolio_two.jpg", //384*260
        "portfolio_three" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/portfolio_three.jpg", //384*260
        "portfolio_four" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/portfolio_four.jpg", //384*260
        "portfolio_five" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/portfolio_five.jpg", //384*260
        "portfolio_six" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/portfolio_six.jpg", //384*260
        "portfolio_seven" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/portfolio_seven.jpg", //384*260
        "portfolio_eight" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/portfolio_eight.jpg", //384*260
        "portfolio_nine" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/portfolio_nine.jpg", //384*260
        "portfolio_ten" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/portfolio_ten.jpg", //384*260
        "releated_portfolio_one" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/releated_portfolio_one.jpg", //438*438
        "releated_portfolio_two" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/releated_portfolio_two.jpg", //438*438
        "releated_portfolio_three" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/releated_portfolio_three.jpg", //438*438
        "releated_portfolio_four" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/releated_portfolio_four.jpg", //438*438
        "releated_portfolio_five" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/releated_portfolio_five.jpg", //438*438
        "releated_portfolio_six" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/releated_portfolio_six.jpg", //438*438
        "releated_portfolio_seven" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/releated_portfolio_seven.jpg", //438*438
        "releated_portfolio_eight" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/releated_portfolio_eight.jpg", //438*438
        "comment_picture_one" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/comment_picture_one.jpg", //107*107
        "comment_picture_two" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/comment_picture_two.jpg", //107*107
        "comment_picture_three" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/comment_picture_three.jpg", //107*107
        "comment_picture_four" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/comment_picture_four.jpg", //107*107
        "mini_cart_one" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/mini_cart_one.jpg", //64*64
        "mini_cart_two" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/mini_cart_two.jpg", //64*64
        "mini_cart_three" => "http://rubidiumstyle.com/theme-forest-connect/torta-kj2mnp-private/mini_cart_three.jpg" //64*64
    );
}else{
    $pictures = array(
        "logo" => "assets/images/logo.png", //118*187
        "avatar_picture_one" => "http://dummyimage.com/360x360/000/fff", //360*360
        "avatar_picture_two" => "http://dummyimage.com/360x360/000/fff", //360*360
        "avatar_picture_three" => "http://dummyimage.com/360x360/000/fff", //360*360
        "avatar_picture_four" => "http://dummyimage.com/360x360/000/fff", //360*36
        "rs_one" => "http://dummyimage.com/588x588/000/fff", //588*588
        "rs_two" => "http://dummyimage.com/1263x500/000/fff", //1263*500
        "about_us_one" => "http://dummyimage.com/588x588/000/fff", //588*588
        "blog_picture_one" => "http://dummyimage.com/1152x455/000/fff", //1152*455
        "blog_picture_two" => "http://dummyimage.com/1152x455/000/fff", //1152*455
        "blog_picture_three" => "http://dummyimage.com/1152x455/000/fff", //1152*455
        "blog_picture_four" => "http://dummyimage.com/1152x455/000/fff", //1152*455
        "marsonry_blog_picture_one" => "http://dummyimage.com/264x264/000/fff", //264*264
        "marsonry_blog_picture_two" => "http://dummyimage.com/264x264/000/fff", //264*264
        "marsonry_blog_picture_three" => "http://dummyimage.com/264x264/000/fff", //264*264
        "marsonry_blog_picture_four" => "http://dummyimage.com/264x264/000/fff", //264*264
        "marsonry_blog_picture_five" => "http://dummyimage.com/264x264/000/fff", //264*264
        "marsonry_blog_picture_six" => "http://dummyimage.com/264x264/000/fff", //264*264
        "marsonry_blog_picture_seven" => "http://dummyimage.com/264x264/000/fff", //264*264
        "marsonry_blog_picture_eight" => "http://dummyimage.com/264x264/000/fff", //264*264
        "marsonry_blog_picture_nine" => "http://dummyimage.com/264x264/000/fff", //264*264
        "marsonry_blog_picture_ten" => "http://dummyimage.com/264x264/000/fff", //264*264
        "testimonials" => "http://dummyimage.com/1680x505/000/fff", //1680*505
        "single_product_one" => "http://dummyimage.com/466x466/000/fff", //466*466
        "portfolio_one" => "http://dummyimage.com/384x260/000/fff", //384*260
        "portfolio_two" => "http://dummyimage.com/384x260/000/fff", //384*260
        "portfolio_three" => "http://dummyimage.com/384x260/000/fff", //384*260
        "portfolio_four" => "http://dummyimage.com/384x260/000/fff", //384*260
        "portfolio_five" => "http://dummyimage.com/384x260/000/fff", //384*260
        "portfolio_six" => "http://dummyimage.com/384x260/000/fff", //384*260
        "portfolio_seven" => "http://dummyimage.com/384x260/000/fff", //384*260
        "portfolio_eight" => "http://dummyimage.com/384x260/000/fff", //384*260
        "portfolio_nine" => "http://dummyimage.com/384x260/000/fff", //384*260
        "portfolio_ten" => "http://dummyimage.com/384x260/000/fff", //384*260
        "releated_portfolio_one" => "http://dummyimage.com/438x438/000/fff", //438*438
        "releated_portfolio_two" => "http://dummyimage.com/438x438/000/fff", //438*438
        "releated_portfolio_three" => "http://dummyimage.com/438x438/000/fff", //438*438
        "releated_portfolio_four" => "http://dummyimage.com/438x438/000/fff", //438*438
        "releated_portfolio_five" => "http://dummyimage.com/438x438/000/fff", //438*438
        "releated_portfolio_six" => "http://dummyimage.com/438x438/000/fff", //438*438
        "releated_portfolio_seven" => "http://dummyimage.com/438x438/000/fff", //438*438
        "releated_portfolio_eight" => "http://dummyimage.com/438x438/000/fff", //438*438
        "comment_picture_one" => "http://dummyimage.com/107x107/000/fff", //107*107
        "comment_picture_two" => "http://dummyimage.com/107x107/000/fff", //107*107
        "comment_picture_three" => "http://dummyimage.com/107x107/000/fff", //107*107
        "comment_picture_four" => "http://dummyimage.com/107x107/000/fff", //107*107
        "mini_cart_one" => "http://dummyimage.com/64x64/000/fff", //64*64
        "mini_cart_two" => "http://dummyimage.com/64x64/000/fff", //64*64
        "mini_cart_three" => "http://dummyimage.com/64x64/000/fff" //64*64
    );
}
?>
<?php include "../elements/head/head.php"; ?>
    <section class="content">
<?php $scroll = " wow fadeInUp animated"; ?>
