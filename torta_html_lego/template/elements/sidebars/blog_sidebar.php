<aside class="widgets">
    <?php include "../elements/contents/widgets/search_widget.php"; ?>
    <?php include "../elements/contents/widgets/archive_widget.php"; ?>
    <?php include "../elements/contents/widgets/categories_widget.php"; ?>
    <?php include "../elements/contents/widgets/comments_list_widget.php"; ?>
    <?php include "../elements/contents/widgets/latest_products_widget.php"; ?>
    <?php include "../elements/contents/widgets/recent_posts_widget.php"; ?>
    <?php include "../elements/contents/widgets/tag_cloud_widget.php"; ?>
</aside>
