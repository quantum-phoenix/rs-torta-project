<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Scrollreveal.js Experimental</title>

    <link rel="stylesheet" type="text/css" href="../bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
    <style type="text/css">
        .align-box:before, .align-box:after {
            content: " ";
        / / 1 display : table;
        / / 2
        }

        .align-box:after {
            clear: both;
        }

        .align-left, .align-right {
        }

        .align-left {
            float: left;
        }

        .align-right {
            float: right;
        }

        .big-box {
            margin-bottom: 32px;
            background: lightblue;
            padding: 32px;
        }

        p {
            line-height: 32px;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <div class="big-box" data-sr="enter left, hustle 20px">
                <p>Lorem ipsum dolor sit amet, sit ullamcorper ligula eu metus, viverra diam dui massa ultricies,
                    dolores sollicitudin proin sit, id convallis a mauris ullamcorper, vel feugiat magna consectetuer
                    quis purus. Tellus in curabitur ultrices vivamus non orci, erat adipiscing a lorem montes. Vivamus
                    ut sit tempor euismod. Magna ipsum purus justo sit eget, morbi odio euismod, quisque ante suscipit
                    pede, morbi auctor metus pretium sit et, et tortor dolor magna. Metus at bibendum libero tortor amet
                    venenatis, dolor pellentesque amet dis in lacus, sapien odio nulla eu ultricies, sodales ut
                    suscipit, class sem ipsum diam. Eros amet non pretium libero elit, orci wisi mi. Vel arcu felis ut
                    eu, potenti sit molestie odio massa nam, vitae fusce bibendum mauris nulla, tortor justo. Aptent
                    vitae, ut mauris a donec arcu, dolor donec. Lectus donec facilisis gravida elit ac risus, iaculis
                    massa scelerisque posuere ultricies felis sagittis, porta rutrum mauris, libero leo erat dolor
                    donec, ut libero faucibus cum diam. Justo orci nec proin, bibendum molestias ligula in metus aenean
                    arcu, volutpat egestas at vitae mi, nulla pellentesque magna neque ut, consectetuer lacinia a
                    hymenaeos placerat consectetuer tortor. Nam rhoncus per sed eleifend vivamus, donec magna sit
                    aperiam dui nec, posuere dignissim, pellentesque fusce orci tortor magna ut orci. Dolor nam neque
                    nec id, rhoncus nec penatibus libero ante gravida, auctor cras nonummy quam luctus est mauris, porta
                    velit, mauris fugiat consequat duis.</p>
            </div>

            <div class="big-box" data-sr="enter left, hustle 20px">
                <p>Lorem ipsum dolor sit amet, sit ullamcorper ligula eu metus, viverra diam dui massa ultricies,
                    dolores sollicitudin proin sit, id convallis a mauris ullamcorper, vel feugiat magna consectetuer
                    quis purus. Tellus in curabitur ultrices vivamus non orci, erat adipiscing a lorem montes. Vivamus
                    ut sit tempor euismod. Magna ipsum purus justo sit eget, morbi odio euismod, quisque ante suscipit
                    pede, morbi auctor metus pretium sit et, et tortor dolor magna. Metus at bibendum libero tortor amet
                    venenatis, dolor pellentesque amet dis in lacus, sapien odio nulla eu ultricies, sodales ut
                    suscipit, class sem ipsum diam. Eros amet non pretium libero elit, orci wisi mi. Vel arcu felis ut
                    eu, potenti sit molestie odio massa nam, vitae fusce bibendum mauris nulla, tortor justo. Aptent
                    vitae, ut mauris a donec arcu, dolor donec. Lectus donec facilisis gravida elit ac risus, iaculis
                    massa scelerisque posuere ultricies felis sagittis, porta rutrum mauris, libero leo erat dolor
                    donec, ut libero faucibus cum diam. Justo orci nec proin, bibendum molestias ligula in metus aenean
                    arcu, volutpat egestas at vitae mi, nulla pellentesque magna neque ut, consectetuer lacinia a
                    hymenaeos placerat consectetuer tortor. Nam rhoncus per sed eleifend vivamus, donec magna sit
                    aperiam dui nec, posuere dignissim, pellentesque fusce orci tortor magna ut orci. Dolor nam neque
                    nec id, rhoncus nec penatibus libero ante gravida, auctor cras nonummy quam luctus est mauris, porta
                    velit, mauris fugiat consequat duis.</p>
            </div>

            <div class="big-box" data-sr="enter left, hustle 20px">
                <p>Lorem ipsum dolor sit amet, sit ullamcorper ligula eu metus, viverra diam dui massa ultricies,
                    dolores sollicitudin proin sit, id convallis a mauris ullamcorper, vel feugiat magna consectetuer
                    quis purus. Tellus in curabitur ultrices vivamus non orci, erat adipiscing a lorem montes. Vivamus
                    ut sit tempor euismod. Magna ipsum purus justo sit eget, morbi odio euismod, quisque ante suscipit
                    pede, morbi auctor metus pretium sit et, et tortor dolor magna. Metus at bibendum libero tortor amet
                    venenatis, dolor pellentesque amet dis in lacus, sapien odio nulla eu ultricies, sodales ut
                    suscipit, class sem ipsum diam. Eros amet non pretium libero elit, orci wisi mi. Vel arcu felis ut
                    eu, potenti sit molestie odio massa nam, vitae fusce bibendum mauris nulla, tortor justo. Aptent
                    vitae, ut mauris a donec arcu, dolor donec. Lectus donec facilisis gravida elit ac risus, iaculis
                    massa scelerisque posuere ultricies felis sagittis, porta rutrum mauris, libero leo erat dolor
                    donec, ut libero faucibus cum diam. Justo orci nec proin, bibendum molestias ligula in metus aenean
                    arcu, volutpat egestas at vitae mi, nulla pellentesque magna neque ut, consectetuer lacinia a
                    hymenaeos placerat consectetuer tortor. Nam rhoncus per sed eleifend vivamus, donec magna sit
                    aperiam dui nec, posuere dignissim, pellentesque fusce orci tortor magna ut orci. Dolor nam neque
                    nec id, rhoncus nec penatibus libero ante gravida, auctor cras nonummy quam luctus est mauris, porta
                    velit, mauris fugiat consequat duis.</p>
            </div>

            <div class="big-box" data-sr="enter left, hustle 20px">
                <p>Lorem ipsum dolor sit amet, sit ullamcorper ligula eu metus, viverra diam dui massa ultricies,
                    dolores sollicitudin proin sit, id convallis a mauris ullamcorper, vel feugiat magna consectetuer
                    quis purus. Tellus in curabitur ultrices vivamus non orci, erat adipiscing a lorem montes. Vivamus
                    ut sit tempor euismod. Magna ipsum purus justo sit eget, morbi odio euismod, quisque ante suscipit
                    pede, morbi auctor metus pretium sit et, et tortor dolor magna. Metus at bibendum libero tortor amet
                    venenatis, dolor pellentesque amet dis in lacus, sapien odio nulla eu ultricies, sodales ut
                    suscipit, class sem ipsum diam. Eros amet non pretium libero elit, orci wisi mi. Vel arcu felis ut
                    eu, potenti sit molestie odio massa nam, vitae fusce bibendum mauris nulla, tortor justo. Aptent
                    vitae, ut mauris a donec arcu, dolor donec. Lectus donec facilisis gravida elit ac risus, iaculis
                    massa scelerisque posuere ultricies felis sagittis, porta rutrum mauris, libero leo erat dolor
                    donec, ut libero faucibus cum diam. Justo orci nec proin, bibendum molestias ligula in metus aenean
                    arcu, volutpat egestas at vitae mi, nulla pellentesque magna neque ut, consectetuer lacinia a
                    hymenaeos placerat consectetuer tortor. Nam rhoncus per sed eleifend vivamus, donec magna sit
                    aperiam dui nec, posuere dignissim, pellentesque fusce orci tortor magna ut orci. Dolor nam neque
                    nec id, rhoncus nec penatibus libero ante gravida, auctor cras nonummy quam luctus est mauris, porta
                    velit, mauris fugiat consequat duis.</p>
            </div>

            <div class="big-box" data-sr="wait 0.2s, scale up 30%">
                <p>Lorem ipsum dolor sit amet, sit ullamcorper ligula eu metus, viverra diam dui massa ultricies,
                    dolores sollicitudin proin sit, id convallis a mauris ullamcorper, vel feugiat magna consectetuer
                    quis purus. Tellus in curabitur ultrices vivamus non orci, erat adipiscing a lorem montes. Vivamus
                    ut sit tempor euismod. Magna ipsum purus justo sit eget, morbi odio euismod, quisque ante suscipit
                    pede, morbi auctor metus pretium sit et, et tortor dolor magna. Metus at bibendum libero tortor amet
                    venenatis, dolor pellentesque amet dis in lacus, sapien odio nulla eu ultricies, sodales ut
                    suscipit, class sem ipsum diam. Eros amet non pretium libero elit, orci wisi mi. Vel arcu felis ut
                    eu, potenti sit molestie odio massa nam, vitae fusce bibendum mauris nulla, tortor justo. Aptent
                    vitae, ut mauris a donec arcu, dolor donec. Lectus donec facilisis gravida elit ac risus, iaculis
                    massa scelerisque posuere ultricies felis sagittis, porta rutrum mauris, libero leo erat dolor
                    donec, ut libero faucibus cum diam. Justo orci nec proin, bibendum molestias ligula in metus aenean
                    arcu, volutpat egestas at vitae mi, nulla pellentesque magna neque ut, consectetuer lacinia a
                    hymenaeos placerat consectetuer tortor. Nam rhoncus per sed eleifend vivamus, donec magna sit
                    aperiam dui nec, posuere dignissim, pellentesque fusce orci tortor magna ut orci. Dolor nam neque
                    nec id, rhoncus nec penatibus libero ante gravida, auctor cras nonummy quam luctus est mauris, porta
                    velit, mauris fugiat consequat duis.</p>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript" charset="utf-8" src="../bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" charset="utf-8" src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" charset="utf-8"
        src="../bower_components/scrollReveal.js/dist/scrollReveal.min.js"></script>
<script>
    window.sr = new scrollReveal();
</script>
</body>
</html>