<?php include "../elements/html_head.php"; ?>

    <div class="container">

        <div class="row">
            <div class="col-md-12">

                <main class="page-content-type">

                    <?php $heading = "Our story"; ?>
                    <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>

                    <div class="row<?php echo $scroll; ?>">
                        <div class="col-md-6">
                            <img class="responsive-image col-6-img"
                                 src="<?php echo $pictures['about_us_one']; ?>" alt="about_us_one">
                        </div>
                        <div class="col-md-6">
                            <h3>Lorem ipsum dolor</h3>

                            <p>Lorem ipsum dolor sit amet, mea quas qualisque prodesset id, est possim ullamcorper at.
                                Ei per ornatus
                                facilisis deterruisset, soleat utroque maiestatis ei eos. In ridens comprehensam eos,
                                mea dicat diceret
                                suscipit no. Cu consetetur assueverit cotidieque mei, aperiam nominavi ut qui, in his
                                etiam aliquid
                                scriptorem. Cetero epicurei instructior eum id, ut quot viderer appareat his, ad veri
                                detracto antiopam nam.
                                An vis alii modo porro, fuisset eligendi ad eos.</p>

                            <h3>Lorem ipsum dolor</h3>

                            <p>Lorem ipsum dolor sit amet, mea quas qualisque prodesset id, est possim ullamcorper at.
                                Ei per ornatus
                                facilisis deterruisset, soleat utroque maiestatis ei eos. In ridens comprehensam eos,
                                mea dicat diceret
                                suscipit no. Cu consetetur assueverit cotidieque mei, aperiam nominavi ut qui, in his
                                etiam aliquid
                                scriptorem. Cetero epicurei instructior eum id, ut quot viderer appareat his, ad veri
                                detracto antiopam nam.
                                An vis alii modo porro, fuisset eligendi ad eos.</p>
                        </div>
                    </div>

                    <?php $heading = "Our team"; ?>
                    <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>

                    <?php include "../elements/contents/blocks/team_members.php"; ?>

                    <?php $heading = "Who are we"; ?>
                    <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>

                    <div class="row<?php echo $scroll; ?>">
                        <div class="col-md-6">
                            <?php include "../elements/contents/blocks/accordion.php"; ?>
                        </div>
                        <div class="col-md-6">
                            <?php include "../elements/contents/blocks/progress_bars.php"; ?>
                        </div>
                    </div>

                    <?php $heading = "About us video"; ?>
                    <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>

                    <div class="row row-col-no-margin">
                        <div class="col-md-12">
                            <?php include "../elements/contents/blocks/full_with_video.php"; ?>
                        </div>
                    </div>

                </main>

            </div>
        </div>

    </div>

<?php include "../elements/html_footer.php"; ?>