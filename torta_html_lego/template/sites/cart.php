<?php include "../elements/html_head.php"; ?>

<div class="container">

    <?php $heading = "Cart"; ?>
    <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>

    <main class="page-content-type">

        <div class="row">
            <div class="col-md-3">
                <h3>Cart total</h3>
                <ul class="ul-li-list cart-list ul-li-vertical-spacing-sm">
                    <li><span class="left">Subtotal</span><span class="right">$216.00</span></li>
                    <li><span class="left">Shipping and Handling</span><span class="right">Free Shipping</span></li>
                    <li class="divider-vertical"></li>
                    <li><span class="left bold">Subtotal</span><span class="right bold">$216.00</span></li>
                    <li><a href="#" class="btn btn-primary">Checkout</a></li>
                </ul>
            </div>
            <div class="col-md-9">
                <table class="table cart-table">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><a href="#"><i class="fa fa-trash-o"></i></a></td>
                        <td>Lorem ipsum</td>
                        <td>$99</td>
                        <td>
                            <ul class="ul-li-inline ul-li-horizontal-spacing-xs">
                                <li><a class="cart-negative " href="#">-</a></li>
                                <li><input type="text" class="form-control cart-input text-center" value="1"></li>
                                <li><a class="cart-positive " href="#">+</a></li>
                            </ul>
                        </td>
                        <td>$99</td>
                    </tr>
                    <tr>
                        <td><a href="#"><i class="fa fa-trash-o"></i></a></td>
                        <td>Lorem ipsum</td>
                        <td>$99</td>
                        <td>
                            <ul class="ul-li-inline ul-li-horizontal-spacing-xs">
                                <li><a class="cart-negative " href="#">-</a></li>
                                <li><input type="text" class="form-control cart-input text-center" value="1"></li>
                                <li><a class="cart-positive " href="#">+</a></li>
                            </ul>
                        </td>
                        <td>$99</td>
                    </tr>
                    <tr>
                        <td><a href="#"><i class="fa fa-trash-o"></i></a></td>
                        <td>Lorem ipsum</td>
                        <td>$99</td>
                        <td>
                            <ul class="ul-li-inline ul-li-horizontal-spacing-xs">
                                <li><a class="cart-negative " href="#">-</a></li>
                                <li><input type="text" class="form-control cart-input text-center" value="1"></li>
                                <li><a class="cart-positive " href="#">+</a></li>
                            </ul>
                        </td>
                        <td>$99</td>
                    </tr>
                    <tr>
                        <td><a href="#"><i class="fa fa-trash-o"></i></a></td>
                        <td>Lorem ipsum</td>
                        <td>$99</td>
                        <td>
                            <ul class="ul-li-inline ul-li-horizontal-spacing-xs">
                                <li><a class="cart-negative " href="#">-</a></li>
                                <li><input type="text" class="form-control cart-input text-center" value="1"></li>
                                <li><a class="cart-positive " href="#">+</a></li>
                            </ul>
                        </td>
                        <td>$99</td>
                    </tr>
                    <tr>
                        <td><a href="#"><i class="fa fa-trash-o"></i></a></td>
                        <td>Lorem ipsum</td>
                        <td>$99</td>
                        <td>
                            <ul class="ul-li-inline ul-li-horizontal-spacing-xs">
                                <li><a class="cart-negative " href="#">-</a></li>
                                <li><input type="text" class="form-control cart-input text-center" value="1"></li>
                                <li><a class="cart-positive " href="#">+</a></li>
                            </ul>
                        </td>
                        <td>$99</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </main>

</div>

<?php include "../elements/html_footer.php"; ?>


