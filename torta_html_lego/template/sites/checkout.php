<?php include "../elements/html_head.php"; ?>

<div class="container">

    <?php $heading = "Checkout"; ?>
    <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>

    <main class="page-content-type">

        <div class="row">

            <div class="col-md-4">
                <h3>Cart total</h3>

                <ul class="ul-li-list ul-li-vertical-spacing-sm">
                    <li>Orange cake slice (2X): $6.00</li>
                    <li>Chocolate cake slice (2X): $16.00</li>
                    <li>Muffin (3X): $96.00</li>
                    <li>Raspberry Buttermilk Dumplings (1X): $146.00</li>
                    <li class="divider-vertical"></li>
                    <li class="bold">Subtotal: $264.00</li>
                    <li>Shipping Charge: $20.00</li>
                    <li class="divider-vertical"></li>
                    <li class="bold positive">Total: $284.00</li>
                    <li>
                        <select class="selectpicker">
                            <option>Paypal</option>
                            <option>Visa</option>
                            <option>Visa electron</option>
                            <option>Discover</option>
                        </select>
                    </li>
                    <li>
                        <ul class="ul-li-inline ul-li-horizontal-spacing-xs">
                            <li><input type="checkbox"></li>
                            <li><p class="no-margin">Read & Accept Terms</p></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#" class="btn btn-primary btn-md">Checkout</a>
                    </li>
                </ul>
            </div>

            <div class="col-md-8">

                <div class="row row-col-half-margin">
                    <div class="col-md-12">
                        <ul class="ul-li-inline shop-progress">
                            <li class="on">Order</li>
                            <li class="on">Production</li>
                            <li class="on current">Checkout</li>
                            <li class="off">Ship</li>
                        </ul>
                    </div>
                </div>

                <div class="row row-col-half-margin">
                    <div class="col-md-12">
                        <label>Country</label>
                        <select class="selectpicker full-width">
                            <option>EU</option>
                            <option>UK</option>
                            <option>USA</option>
                        </select>
                    </div>
                </div>

                <div class="row row-col-half-margin">
                    <div class="col-md-6">
                        <label>First name</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label>Last name</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="row row-col-half-margin">
                    <div class="col-md-12">
                        <label>Company name</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="row row-col-half-margin">
                    <div class="col-md-12">
                        <label>Address</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="row row-col-half-margin">
                    <div class="col-md-6">
                        <label>E-mail</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-md-6">
                        <label>Phone</label>
                        <input type="text" class="form-control">
                    </div>
                </div>

                <div class="row row-col-no-margin">
                    <div class="col-md-12">
                        <ul class="ul-li-inline ul-li-horizontal-spacing-xs">
                            <li><input type="checkbox"></li>
                            <li><p class="no-margin">Create an account</p></li>
                        </ul>
                    </div>
                </div>

            </div>

        </div>

    </main>

</div>

<?php include "../elements/html_footer.php"; ?>



