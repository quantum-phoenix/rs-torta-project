<?php include "../elements/html_head.php"; ?>

    <div id="google-maps"></div>

    <div class="container">

        <?php $heading = "Contact"; ?>
        <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>

        <div class="row">
            <div class="col-md-12">

                <main class="list-blog-content-type">

                    <div class="row row-col-no-margin">
                        <div class="col-md-8">
                            <form>
                                <div class="row row-col-half-margin">
                                    <div class="col-md-12">
                                        <h3>Vel an adhuc</h3>
                                        <p class="no-margin">Vis tale erroribus an, an tritani tibique corrumpit has. Has velit graece ornatus ad. Qui ea soluta civibus atomorum, ne qui iusto animal. Eleifend omittantur reprehendunt sit at. Ea simul populo est, quem maiorum inciderint vis no. Modo minimum ea sit, quidam conceptam usu ad, vel cu dicant prompta erroribus.</p>
                                    </div>
                                </div>
                                <div class="row row-col-half-margin">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" id="dirstname" class="form-control"
                                                   name="first_name"
                                                   placeholder="First name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" id="lastname" class="form-control"
                                                   name="last_name"
                                                   placeholder="Last name">
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-col-half-margin">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" id="phonenumber" class="form-control"
                                                   name="phone_number"
                                                   placeholder="Phone number">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" id="email" class="form-control"
                                                   name="email_address"
                                                   placeholder="Email address">
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-col-half-margin">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea class="form-control" rows="6"
                                                      placeholder="Your message"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-col-no-margin">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input class="btn btn-primary" type="submit" value="Submit">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4">
                            <div class="google-maps-place">
                                <h3>Get in touch</h3>
                                <p class="bold">Address Office</p>
                                <ul class="ul-li-inline ul-li-horizontal-spacing-xs">
                                    <li><i class="fa fa-building"></i></li>
                                    <li>Address: 123 Main Street, Anytown,CA 12345 USA.</li>
                                </ul>
                                <p class="bold">Phone Number</p>
                                <ul class="ul-li-inline ul-li-horizontal-spacing-xs">
                                    <li><i class="fa fa-phone"></i></li>
                                    <li>(400) 123 456 789 - (800) 123 456 789</li>
                                </ul>
                                <p class="bold">Email address</p>
                                <ul class="ul-li-inline ul-li-horizontal-spacing-xs">
                                    <li><i class="fa fa-envelope"></i></li>
                                    <li>info@domainname.com</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </main>

            </div>

        </div>

    </div>

<?php include "../elements/html_footer.php"; ?>