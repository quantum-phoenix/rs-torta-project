<?php include "../elements/html_head.php"; ?>

<?php include '../elements/contents/blocks/revolution_slider.php'; ?>

    <div class="div-content-container container">
        <div class="row">
            <div class="col-md-12">
                <div class="home-content-type">

                    <?php $heading = "Services Box"; ?>
                    <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>
                    <?php include '../elements/contents/blocks/services_box.php'; ?>

                    <?php $heading = "Team Members"; ?>
                    <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>
                    <?php include '../elements/contents/blocks/team_members.php'; ?>


                    <?php $heading = "Latest Products"; ?>
                    <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>
                    <?php include '../elements/contents/blocks/latest_products.php'; ?>

                    <?php $heading = "Clients"; ?>
                    <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>
                    <?php include '../elements/contents/blocks/clients.php'; ?>


                </div>
            </div>
        </div>
    </div>

    <div class="testimonials-content-container">
        <div class="testimonials-content-type">

            <?php $heading = "Testimonials"; ?>
            <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>
            <?php include '../elements/contents/blocks/testimonials.php'; ?>

        </div>
    </div>

    <div class="div-content-container container">
        <div class="row">
            <div class="col-md-12">
                <div class="home-content-type">

                    <?php $heading = "Releated Portfolio"; ?>
                    <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>
                    <?php include '../elements/contents/blocks/releated_gallery.php'; ?>

                </div>
            </div>
        </div>
    </div>

<?php include "../elements/html_footer.php"; ?>