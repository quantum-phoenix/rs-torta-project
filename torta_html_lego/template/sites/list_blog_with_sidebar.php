<?php include "../elements/html_head.php"; ?>

    <div class="container">

        <?php $heading = "Blog With Sidebar"; ?>
        <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>

        <div class="row">
            <div class="col-md-9">

                <main class="list-blog-content-type">

                    <article class="post entry list-blog-element standard-post-format<?php echo $scroll; ?>">
                        <div class="entry-image thumbnail-image">

                            <div class="int-image-hover-parent">
                                <img class="responsive-image" src="<?php echo $pictures['blog_picture_one']; ?>"
                                     alt="blog_picture_one"/>

                                <div class="int-image-hover">
                                    <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>">
                                        <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                    </a>
                                </div>
                            </div>

                        </div>
                        <div class="entry-details">
                                        <span class="entry-details-author">
                                            <i class="fa fa-user"></i>
                                            By Admin
                                        </span>
                                        <span class="entry-details-date">
                                            <i class="fa fa-clock-o"></i>
                                            May 29th,2017
                                        </span>
                                        <span class="entry-details-comment">
                                            <i class="fa fa-comments-o"></i>
                                            5 Comments
                                        </span>
                                        <span class="entry-details-tag">
                                            <i class="fa fa-cloud"></i>
                                            Images, Photography
                                        </span>
                        </div>
                        <div class="entry-title">
                            <h2>Standard blog post</h2>
                        </div>
                        <div class="entry-excerpt">
                            <p>Sed gravida laoreet venenatis pede, metus gravida amet laoreet maecenas tincidunt
                                massa. Viverra mauris vitae ipsum tempor vestibulum felis, sit ultricies cursus,
                                justo nulla lacus sit suspendisse cursus, curabitur porta nam at consequat molestie
                                gravida, libero morbi arcu</p>
                        </div>
                        <div class="entry-read-more">
                            <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>" class="btn btn-primary">Read More</a>
                        </div>
                    </article>

                    <article class="post entry list-blog-element standard-post-format">
                        <div class="entry-image thumbnail-image">

                            <div class="int-image-hover-parent">
                                <img class="responsive-image" src="<?php echo $pictures['blog_picture_four']; ?>"
                                     alt="blog_picture_one"/>

                                <div class="int-image-hover">
                                    <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>">
                                        <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                    </a>
                                </div>
                            </div>

                        </div>
                        <div class="entry-details">
                                        <span class="entry-details-author">
                                            <i class="fa fa-user"></i>
                                            By Admin
                                        </span>
                                        <span class="entry-details-date">
                                            <i class="fa fa-clock-o"></i>
                                            May 29th,2017
                                        </span>
                                        <span class="entry-details-comment">
                                            <i class="fa fa-comments-o"></i>
                                            5 Comments
                                        </span>
                                        <span class="entry-details-tag">
                                            <i class="fa fa-cloud"></i>
                                            Images, Photography
                                        </span>
                        </div>
                        <div class="entry-title">
                            <h2>Standard blog post</h2>
                        </div>
                        <div class="entry-excerpt">
                            <p>Sed gravida laoreet venenatis pede, metus gravida amet laoreet maecenas tincidunt
                                massa. Viverra mauris vitae ipsum tempor vestibulum felis, sit ultricies cursus,
                                justo nulla lacus sit suspendisse cursus, curabitur porta nam at consequat molestie
                                gravida, libero morbi arcu</p>
                        </div>
                        <div class="entry-read-more">
                            <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>" class="btn btn-primary">Read More</a>
                        </div>
                    </article>

                    <article class="post entry list-blog-element standard-post-format<?php echo $scroll; ?>">
                        <div class="entry-image thumbnail-image slick-slider-single">
                            <div class="slick-slider-item">
                                <img class="responsive-image" src="<?php echo $pictures['blog_picture_two']; ?>"
                                     alt="picture2"/>
                            </div>
                            <div class="slick-slider-item">
                                <img class="responsive-image" src="<?php echo $pictures['blog_picture_three']; ?>"
                                     alt="picture3"/>
                            </div>
                        </div>
                        <div class="entry-details">
                                        <span class="entry-details-author">
                                            <i class="fa fa-user"></i>
                                            By Admin
                                        </span>
                                        <span class="entry-details-date">
                                            <i class="fa fa-clock-o"></i>
                                            May 29th,2017
                                        </span>
                                        <span class="entry-details-comment">
                                            <i class="fa fa-comments-o"></i>
                                            5 Comments
                                        </span>
                                        <span class="entry-details-tag">
                                            <i class="fa fa-cloud"></i>
                                            Images, Photography
                                        </span>
                        </div>
                        <div class="entry-title">
                            <h2>Slider blog post</h2>
                        </div>
                        <div class="entry-excerpt">
                            <p>Sed gravida laoreet venenatis pede, metus gravida amet laoreet maecenas tincidunt
                                massa. Viverra mauris vitae ipsum tempor vestibulum felis, sit ultricies cursus,
                                justo nulla lacus sit suspendisse cursus, curabitur porta nam at consequat molestie
                                gravida, libero morbi arcu</p>
                        </div>
                        <div class="entry-read-more">
                            <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>" class="btn btn-primary">Read More</a>
                        </div>
                    </article>

                    <article class="post entry list-blog-element quote-post-format<?php echo $scroll; ?>">
                        <div class="entry-details">
                                        <span class="entry-details-author">
                                            <i class="fa fa-user"></i>
                                            By Admin
                                        </span>
                                        <span class="entry-details-date">
                                            <i class="fa fa-clock-o"></i>
                                            May 29th,2017
                                        </span>
                                        <span class="entry-details-comment">
                                            <i class="fa fa-comments-o"></i>
                                            1  Comment
                                        </span>
                                        <span class="entry-details-tag">
                                            <i class="fa fa-cloud"></i>
                                            Images, Photography
                                        </span>
                        </div>
                        <div class="entry-title">
                            <h2>Slider blog post</h2>
                        </div>
                        <div class="entry-excerpt">
                            <blockquote>Sed gravida laoreet venenatis pede, metus gravida amet laoreet maecenas
                                tincidunt massa. Viverra mauris vitae ipsum tempor vestibulum felis, sit ultricies
                                cursus, justo nulla lacus sit suspendisse cursus, curabitur porta nam at consequat
                                molestie gravida, libero morbi arcu
                            </blockquote>
                        </div>
                        <div class="entry-read-more">
                            <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>" class="btn btn-primary">Read More</a>
                        </div>
                    </article>

                    <article class="post entry list-blog-element video-post-format<?php echo $scroll; ?>">

                        <div class="entry-video">
                            <iframe src="https://www.youtube.com/embed/vw_h7kSJlqg?modestbranding=1&autoplay=0&showinfo=0&controls=0"
                                    allowfullscreen></iframe>
                        </div>
                        <div class="entry-details list-blog-entry-details">
                                        <span class="entry-details-author">
                                            <i class="fa fa-user"></i>
                                            By Admin
                                        </span>
                                        <span class="entry-details-date">
                                            <i class="fa fa-clock-o"></i>
                                            May 29th,2017
                                        </span>
                                        <span class="entry-details-comment">
                                            <i class="fa fa-comments-o"></i>
                                            5 Comments
                                        </span>
                                        <span class="entry-details-tag">
                                            <i class="fa fa-cloud"></i>
                                            Images, Photography
                                        </span>
                        </div>
                        <div class="entry-title">
                            <h2>Slider blog post</h2>
                        </div>
                        <div class="entry-excerpt">
                            <p>Sed gravida laoreet venenatis pede, metus gravida amet laoreet maecenas tincidunt
                                massa. Viverra mauris vitae ipsum tempor vestibulum felis, sit ultricies cursus,
                                justo nulla lacus sit suspendisse cursus, curabitur porta nam at consequat molestie
                                gravida, libero morbi arcu</p>
                        </div>
                        <div class="entry-read-more">
                            <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>" class="btn btn-primary">Read More</a>
                        </div>
                    </article>

                    <?php include "../elements/contents/blocks/pagination.php"; ?>

                </main>


            </div>

            <div class="col-md-3">
                <?php include "../elements/sidebars/blog_sidebar.php"; ?>
            </div>
        </div>

    </div>

<?php include "../elements/html_footer.php"; ?>