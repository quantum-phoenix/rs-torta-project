<?php include "../elements/html_head.php"; ?>

    <div class="container">

        <?php $heading = "Masonry Blog"; ?>
        <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>

        <div class="row">
            <div class="col-md-12">

                <main class="masonry-blog-content-type">

                    <article class="masonry-blog-element post entry standard-post-format four-column-masonry-element">
                        <div class="same-side-force">
                            <h2>May</h2>

                            <h3>10</h3>
                        </div>
                        <div class="entry-image thumbnail-image">
                            <div class="int-image-hover-parent">
                                <img src="<?php echo $pictures['marsonry_blog_picture_one']; ?>"
                                     alt="marsonry_blog_picture_one">

                                <div class="int-image-hover">
                                    <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>">
                                        <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="entry-details">
                            <span class="entry-details-author">
                                <i class="fa fa-user"></i>
                                By Admin
                            </span>
                            <span class="entry-details-comment">
                                <i class="fa fa-comments-o"></i>
                                5 Comments
                            </span>
                        </div>
                        <div class="entry-title">
                            <h3>Example head</h3>
                        </div>
                        <div class="entry-excerpt">
                            <p>Nunc varius nisi eu eros imperdiet consectetur. Nam elit nunc, auctor gravida arcu quis,
                                volutpat
                                pellentesque...</p>
                        </div>
                        <div class="entry-read-more">
                            <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>"> class="btn btn-primary">Read More</a>
                        </div>
                    </article>

                    <article class="masonry-blog-element post entry quote-post-format four-column-masonry-element">
                        <div class="entry-details">
                            <span class="entry-details-author">
                                <i class="fa fa-user"></i>
                                By Admin
                            </span>
                            <span class="entry-details-comment">
                                <i class="fa fa-comments-o"></i>
                                5 Comments
                            </span>
                        </div>
                        <div class="entry-title">
                            <h3>Example head</h3>
                        </div>
                        <div class="entry-excerpt">
                            <blockquote>Sed gravida laoreet venenatis pede, metus gravida amet laoreet maecenas
                                tincidunt
                                massa.
                                Viverra mauris vitae ipsum tempor vestibulum felis, sit ultricies cursus, justo nulla
                                lacus
                                sit
                                suspendisse cursus, curabitur porta nam at consequat molestie gravida, libero morbi arcu
                            </blockquote>
                        </div>
                        <div class="entry-read-more">
                            <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>" class="btn btn-primary">Read More</a>
                        </div>
                    </article>

                    <article class="masonry-blog-element post entry standard-post-format four-column-masonry-element">
                        <div class="same-side-force">
                            <h2>May</h2>

                            <h3>16</h3>
                        </div>
                        <div class="entry-image thumbnail-image">
                            <div class="int-image-hover-parent">
                                <img src="<?php echo $pictures['marsonry_blog_picture_five']; ?>"
                                     alt="marsonry-blog-picture-4">

                                <div class="int-image-hover">
                                    <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>">
                                        <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="entry-details">
                            <span class="entry-details-author">
                                <i class="fa fa-user"></i>
                                By Admin
                            </span>
                            <span class="entry-details-comment">
                                <i class="fa fa-comments-o"></i>
                                5 Comments
                            </span>
                        </div>
                        <div class="entry-title">
                            <h3>Example head</h3>
                        </div>
                        <div class="entry-excerpt">
                            <p>Nunc varius nisi eu eros imperdiet consectetur. Nam elit nunc, auctor gravida arcu quis,
                                volutpat
                                pellentesque...</p>
                        </div>
                        <div class="entry-read-more">
                            <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>" class="btn btn-primary">Read More</a>
                        </div>
                    </article>

                    <article class="masonry-blog-element post entry standard-post-format four-column-masonry-element">
                        <div class="same-side-force">
                            <h2>May</h2>

                            <h3>16</h3>
                        </div>
                        <div class="entry-image thumbnail-image">
                            <div class="int-image-hover-parent">
                                <img src="<?php echo $pictures['marsonry_blog_picture_six']; ?>"
                                     alt="marsonry-blog-picture-4">

                                <div class="int-image-hover">
                                    <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>">
                                        <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="entry-details">
                            <span class="entry-details-author">
                                <i class="fa fa-user"></i>
                                By Admin
                            </span>
                            <span class="entry-details-comment">
                                <i class="fa fa-comments-o"></i>
                                5 Comments
                            </span>
                        </div>
                        <div class="entry-title">
                            <h3>Example head</h3>
                        </div>
                        <div class="entry-excerpt">
                            <p>Nunc varius nisi eu eros imperdiet consectetur. Assum fastidii adipiscing ei cum. Nam
                                elit
                                nunc, auctor gravida arcu quis, volutpat
                                pellentesque...</p>
                        </div>
                        <div class="entry-read-more">
                            <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>" class="btn btn-primary">Read More</a>
                        </div>
                    </article>

                    <article class="masonry-blog-element post entry standard-post-format four-column-masonry-element">
                        <div class="same-side-force">
                            <h2>May</h2>

                            <h3>16</h3>
                        </div>
                        <div class="entry-image thumbnail-image">
                            <div class="int-image-hover-parent">
                                <img src="<?php echo $pictures['marsonry_blog_picture_seven']; ?>"
                                     alt="marsonry-blog-picture-4">

                                <div class="int-image-hover">
                                    <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>">
                                        <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="entry-details">
                            <span class="entry-details-author">
                                <i class="fa fa-user"></i>
                                By Admin
                            </span>
                            <span class="entry-details-comment">
                                <i class="fa fa-comments-o"></i>
                                5 Comments
                            </span>
                        </div>
                        <div class="entry-title">
                            <h3>Example head</h3>
                        </div>
                        <div class="entry-excerpt">
                            <p>Nunc varius nisi eu eros imperdiet consectetur. Nam elit nunc, auctor gravida arcu quis,
                                volutpat
                                pellentesque...</p>
                        </div>
                        <div class="entry-read-more">
                            <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>" class="btn btn-primary">Read More</a>
                        </div>
                    </article>

                    <article class="masonry-blog-element post entry standard-post-format four-column-masonry-element">
                        <div class="same-side-force">
                            <h2>May</h2>

                            <h3>16</h3>
                        </div>
                        <div class="entry-image thumbnail-image">
                            <div class="int-image-hover-parent">
                                <img src="<?php echo $pictures['marsonry_blog_picture_eight']; ?>"
                                     alt="marsonry-blog-picture-4">

                                <div class="int-image-hover">
                                    <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>">
                                        <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="entry-details">
                            <span class="entry-details-author">
                                <i class="fa fa-user"></i>
                                By Admin
                            </span>
                            <span class="entry-details-comment">
                                <i class="fa fa-comments-o"></i>
                                5 Comments
                            </span>
                        </div>
                        <div class="entry-title">
                            <h3>Example head</h3>
                        </div>
                        <div class="entry-excerpt">
                            <p>Nunc varius nisi eu eros imperdiet consectetur. Nam elit nunc, auctor gravida arcu quis,
                                volutpat
                                pellentesque...</p>
                        </div>
                        <div class="entry-read-more">
                            <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>" class="btn btn-primary">Read More</a>
                        </div>
                    </article>

                    <article class="masonry-blog-element post entry standard-post-format four-column-masonry-element">
                        <div class="same-side-force">
                            <h2>May</h2>

                            <h3>11</h3>
                        </div>
                        <div class="entry-image thumbnail-image slick-slider-single">
                            <div class="slick-slider-item">
                                <img src="<?php echo $pictures['marsonry_blog_picture_three']; ?>"
                                     alt="marsonry-blog-picture-1">
                            </div>
                            <div class="slick-slider-item">
                                <img src="<?php echo $pictures['marsonry_blog_picture_two']; ?>"
                                     alt="marsonry-blog-picture-1">
                            </div>
                        </div>
                        <div class="entry-details">
                            <span class="entry-details-author">
                                <i class="fa fa-user"></i>
                                By Admin
                            </span>
                            <span class="entry-details-comment">
                                <i class="fa fa-comments-o"></i>
                                5 Comments
                            </span>
                        </div>
                        <div class="entry-title">
                            <h3>Example head</h3>
                        </div>
                        <div class="entry-excerpt">
                            <p>Nunc varius nisi eu eros imperdiet consectetur. Nam elit nunc, auctor gravida arcu quis,
                                volutpat
                                pellentesque...</p>
                        </div>
                        <div class="entry-read-more">
                            <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>" class="btn btn-primary">Read More</a>
                        </div>
                    </article>

                    <article class="masonry-blog-element post entry video-post-format four-column-masonry-element">
                        <div class="same-side-force">
                            <h2>May</h2>

                            <h3>14</h3>
                        </div>
                        <div class="entry-video">
                            <iframe src="https://www.youtube.com/embed/vw_h7kSJlqg?modestbranding=1&autoplay=0&showinfo=0&controls=0"
                                    allowfullscreen></iframe>
                        </div>
                        <div class="entry-details">
                            <span class="entry-details-author">
                                <i class="fa fa-user"></i>
                                By Admin
                            </span>
                            <span class="entry-details-comment">
                                <i class="fa fa-comments-o"></i>
                                5 Comments
                            </span>
                        </div>
                        <div class="entry-title">
                            <h3>Example head</h3>
                        </div>
                        <div class="entry-excerpt">
                            <p>Integer sem amet nullam eleifend, et facilisis, platea suspendisse tempor wisi dui ornare
                                ipsum. Amet
                                lacus at, a mi, aliquet posuere arcu, sit nulla commodi...</p>
                        </div>
                        <div class="entry-read-more">
                            <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>" class="btn btn-primary">Read More</a>
                        </div>
                    </article>

                    <article class="masonry-blog-element post entry standard-post-format four-column-masonry-element">
                        <div class="same-side-force">
                            <h2>May</h2>

                            <h3>16</h3>
                        </div>
                        <div class="entry-image thumbnail-image">
                            <div class="int-image-hover-parent">
                                <img src="<?php echo $pictures['marsonry_blog_picture_four']; ?>"
                                     alt="marsonry-blog-picture-4">

                                <div class="int-image-hover">
                                    <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>">
                                        <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="entry-details">
                            <span class="entry-details-author">
                                <i class="fa fa-user"></i>
                                By Admin
                            </span>
                            <span class="entry-details-comment">
                                <i class="fa fa-comments-o"></i>
                                5 Comments
                            </span>
                        </div>
                        <div class="entry-title">
                            <h3>Example head</h3>
                        </div>
                        <div class="entry-excerpt">
                            <p>Nunc varius nisi eu eros imperdiet consectetur. Nam elit nunc, auctor gravida arcu quis,
                                volutpat
                                pellentesque...</p>
                        </div>
                        <div class="entry-read-more">
                            <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>" class="btn btn-primary">Read More</a>
                        </div>
                    </article>

                    <article class="masonry-blog-element post entry standard-post-format four-column-masonry-element">
                        <div class="same-side-force">
                            <h2>May</h2>

                            <h3>10</h3>
                        </div>
                        <div class="entry-image thumbnail-image">
                            <div class="int-image-hover-parent">
                                <img src="<?php echo $pictures['marsonry_blog_picture_nine']; ?>"
                                     alt="marsonry_blog_picture_one">

                                <div class="int-image-hover">
                                    <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>">
                                        <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="entry-details">
                            <span class="entry-details-author">
                                <i class="fa fa-user"></i>
                                By Admin
                            </span>
                            <span class="entry-details-comment">
                                <i class="fa fa-comments-o"></i>
                                5 Comments
                            </span>
                        </div>
                        <div class="entry-title">
                            <h3>Example head</h3>
                        </div>
                        <div class="entry-excerpt">
                            <p>Nunc varius nisi eu eros imperdiet consectetur. Nam elit nunc, auctor gravida arcu quis,
                                volutpat
                                pellentesque...</p>
                        </div>
                        <div class="entry-read-more">
                            <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>" class="btn btn-primary">Read More</a>
                        </div>
                    </article>

                    <article class="masonry-blog-element post entry standard-post-format four-column-masonry-element">
                        <div class="same-side-force">
                            <h2>May</h2>

                            <h3>10</h3>
                        </div>
                        <div class="entry-image thumbnail-image">
                            <div class="int-image-hover-parent">
                                <img src="<?php echo $pictures['marsonry_blog_picture_ten']; ?>"
                                     alt="marsonry_blog_picture_one">

                                <div class="int-image-hover">
                                    <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>">
                                        <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="entry-details">
                            <span class="entry-details-author">
                                <i class="fa fa-user"></i>
                                By Admin
                            </span>
                            <span class="entry-details-comment">
                                <i class="fa fa-comments-o"></i>
                                5 Comments
                            </span>
                        </div>
                        <div class="entry-title">
                            <h3>Example head</h3>
                        </div>
                        <div class="entry-excerpt">
                            <p>Nunc varius nisi eu eros imperdiet consectetur. Nam elit nunc, auctor gravida arcu quis,
                                volutpat
                                pellentesque...</p>
                        </div>
                        <div class="entry-read-more">
                            <a href="<?php echo !$html_or_php ? "single_post.php" : "single_post.html"; ?>" class="btn btn-primary">Read More</a>
                        </div>
                    </article>

                </main>

            </div>

        </div>

    </div>

<?php include "../elements/html_footer.php"; ?>