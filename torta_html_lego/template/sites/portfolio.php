<?php include "../elements/html_head.php"; ?>

    <div class="container">

        <?php $heading = "Portfolio"; ?>
        <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>

        <div class="row">
            <div class="col-md-12">
                <div class="isotope-filters">
                    <ul>
                        <li>
                            <button class="btn btn-primary is-checked" data-filter="*">All</button>
                        </li>
                        <li>
                            <button class="btn btn-primary" data-filter=".cat-muffin">Muffin</button>
                        </li>
                        <li>
                            <button class="btn btn-primary" data-filter=".cat-meringue">Meringue</button>
                        </li>
                        <li>
                            <button class="btn btn-primary" data-filter=".cat-cake">Cake</button>
                        </li>
                        <li>
                            <button class="btn btn-primary" data-filter=".cat-sweet">Sweet</button>
                        </li>
                        <li>
                            <button class="btn btn-primary" data-filter=".cat-diabetic">Diabetic</button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row row-col-no-margin">
            <div class="col-md-12">
                <div class="portfolio-content-type">

                    <div class="entry portfolio-element cat-sweet cat-cake">
                        <div class="entry-image thumbnail-image">
                            <div class="int-image-hover-parent">
                                <img class="portfolio-img responsive-image"
                                     src="<?php echo $pictures['portfolio_one']; ?>" alt="portfolio_one">

                                <div class="int-image-hover">
                                    <a href="<?php echo !$html_or_php ? "single_portfolio.php" : "single_portfolio.html"; ?>">
                                        <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="entry-title entry-title-center">
                            <h3>Celebration cake</h3>
                        </div>
                        <div class="entry-portfolio-tags">
                            <ul>
                                <li><span>Cake</span></li>
                                <li>/</li>
                                <li><span>Sweet</span></li>
                            </ul>
                        </div>
                    </div>

                    <div class="entry portfolio-element cat-sweet cat-cake">
                        <div class="entry-image thumbnail-image">
                            <div class="int-image-hover-parent">
                                <img class="portfolio-img responsive-image"
                                     src="<?php echo $pictures['portfolio_two']; ?>" alt="portfolio_two">

                                <div class="int-image-hover">
                                    <a href="<?php echo !$html_or_php ? "single_portfolio.php" : "single_portfolio.html"; ?>">
                                        <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="entry-title entry-title-center"><h3>Beautiful cake</h3></div>
                        <div class="entry-portfolio-tags">
                            <ul>
                                <li><span class="negative">Cake</span></li>
                                <li>/</li>
                                <li><span class="negative">Sweet</span></li>
                            </ul>
                        </div>
                    </div>

                    <div class="entry portfolio-element cat-sweet cat-cake">
                        <div class="entry-image thumbnail-image">
                            <div class="int-image-hover-parent">
                                <img class="portfolio-img responsive-image"
                                     src="<?php echo $pictures['portfolio_three']; ?>" alt="portfolio_three">

                                <div class="int-image-hover">
                                    <a href="<?php echo !$html_or_php ? "single_portfolio.php" : "single_portfolio.html"; ?>">
                                        <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="entry-title entry-title-center"><h3>Cake slice</h3></div>
                        <div class="entry-portfolio-tags">
                            <ul>
                                <li><span class="negative">Cake</span></li>
                                <li>/</li>
                                <li><span class="negative">Sweet</span></li>
                            </ul>
                        </div>
                    </div>

                    <div class="entry portfolio-element cat-cake cat-diabetic">
                        <div class="entry-image thumbnail-image">
                            <div class="int-image-hover-parent">
                                <img class="portfolio-img responsive-image"
                                     src="<?php echo $pictures['portfolio_four']; ?>" alt="portfolio_four">

                                <div class="int-image-hover">
                                    <a href="<?php echo !$html_or_php ? "single_portfolio.php" : "single_portfolio.html"; ?>">
                                        <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="entry-title entry-title-center"><h3>Pastry</h3></div>
                        <div class="entry-portfolio-tags">
                            <ul>
                                <li><span class="negative">Cake</span></li>
                                <li>/</li>
                                <li><span class="negative">Diabetic</span></li>
                            </ul>
                        </div>
                    </div>

                    <div class="entry portfolio-element cat-sweet cat-meringue">
                        <div class="entry-image thumbnail-image">
                            <div class="int-image-hover-parent">
                                <img class="portfolio-img responsive-image"
                                     src="<?php echo $pictures['portfolio_five']; ?>" alt="portfolio_five">

                                <div class="int-image-hover">
                                    <a href="<?php echo !$html_or_php ? "single_portfolio.php" : "single_portfolio.html"; ?>">
                                        <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="entry-title entry-title-center"><h3>Muffin</h3></div>
                        <div class="entry-portfolio-tags">
                            <ul>
                                <li><span class="negative">Meringue</span></li>
                            </ul>
                        </div>
                    </div>

                    <div class="entry portfolio-element cat-muffin cat-sweet cat-meringue">
                        <div class="entry-image thumbnail-image">
                            <div class="int-image-hover-parent">
                                <img class="portfolio-img responsive-image"
                                     src="<?php echo $pictures['portfolio_six']; ?>" alt="portfolio_six">

                                <div class="int-image-hover">
                                    <a href="<?php echo !$html_or_php ? "single_portfolio.php" : "single_portfolio.html"; ?>">
                                        <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="entry-title entry-title-center"><h3>Muffin</h3></div>
                        <div class="entry-portfolio-tags">
                            <ul>
                                <li><span class="negative">Muffin</span></li>
                                <li>/</li>
                                <li><span class="negative">Sweet</span></li>
                                <li>/</li>
                                <li><span class="negative">Meringue</span></li>
                            </ul>
                        </div>
                    </div>

                    <div class="entry portfolio-element cat-sweet">
                        <div class="entry-image thumbnail-image">
                            <div class="int-image-hover-parent">
                                <img class="portfolio-img responsive-image"
                                     src="<?php echo $pictures['portfolio_seven']; ?>" alt="portfolio_seven">

                                <div class="int-image-hover">
                                    <a href="<?php echo !$html_or_php ? "single_portfolio.php" : "single_portfolio.html"; ?>">
                                        <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="entry-title entry-title-center"><h3>New cake</h3></div>
                        <div class="entry-portfolio-tags">
                            <ul>
                                <li><span class="negative">Sweet</span></li>
                            </ul>
                        </div>
                    </div>

                    <div class="entry portfolio-element cat-muffin cat-sweet">
                        <div class="entry-image thumbnail-image">
                            <div class="int-image-hover-parent">
                                <img class="portfolio-img responsive-image"
                                     src="<?php echo $pictures['portfolio_eight']; ?>" alt="portfolio_eight">

                                <div class="int-image-hover">
                                    <a href="<?php echo !$html_or_php ? "single_portfolio.php" : "single_portfolio.html"; ?>">
                                        <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="entry-title entry-title-center"><h3>Muffin</h3></div>
                        <div class="entry-portfolio-tags">
                            <ul>
                                <li><span class="negative">Muffin</span></li>
                                <li>/</li>
                                <li><span class="negative">Sweet</span></li>
                            </ul>
                        </div>
                    </div>

                    <div class="entry portfolio-element cat-muffin cat-sweet">
                        <div class="entry-image thumbnail-image">
                            <div class="int-image-hover-parent">
                                <img class="portfolio-img responsive-image"
                                     src="<?php echo $pictures['portfolio_nine']; ?>" alt="portfolio_nine">

                                <div class="int-image-hover">
                                    <a href="<?php echo !$html_or_php ? "single_portfolio.php" : "single_portfolio.html"; ?>">
                                        <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="entry-title entry-title-center"><h3>Muffin</h3></div>
                        <div class="entry-portfolio-tags">
                            <ul>
                                <li><span class="negative">Muffin</span></li>
                                <li>/</li>
                                <li><span class="negative">Sweet</span></li>
                            </ul>
                        </div>
                    </div>

                    <div class="entry portfolio-element cat-muffin cat-sweet">
                        <div class="entry-image thumbnail-image">
                            <div class="int-image-hover-parent">
                                <img class="portfolio-img responsive-image"
                                     src="<?php echo $pictures['portfolio_ten']; ?>" alt="portfolio_ten">

                                <div class="int-image-hover">
                                    <a href="<?php echo !$html_or_php ? "single_portfolio.php" : "single_portfolio.html"; ?>">
                                        <div class="i-parent"><i class="fa fa-search-plus fa-5"></i></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="entry-title entry-title-center"><h3>Muffin</h3></div>
                        <div class="entry-portfolio-tags">
                            <ul>
                                <li><span class="negative">Muffin</span></li>
                                <li>/</li>
                                <li><span class="negative">Sweet</span></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>

<?php include "../elements/html_footer.php"; ?>