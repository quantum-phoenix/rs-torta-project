<?php include "../elements/html_head.php"; ?>

    <div class="container">

        <?php $heading = "Shop Without Sidebar"; ?>
        <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>

        <div class="row row-col-half-margin">
            <div class="col-md-12">

                <main class="shop-content-type shop-content-type-without-sidebar">

                    <div class="shop-content-filters half-box-parent">
                        <div class="half-box">
                            <div class="align-box">
                                <div class="align-left">
                                    <p class="no-margin inline-block">Showing all 17 results</p>
                                </div>
                            </div>
                        </div>
                        <div class="half-box">
                            <div class="align-box">
                                <div class="align-right">
                                    <select class="selectpicker">
                                        <option>Default sorting</option>
                                        <option>Sort by popularity</option>
                                        <option>Sort by avarge rating</option>
                                        <option>Sort by newness</option>
                                        <option>Sort by price: low to high</option>
                                        <option>Sort by price: high to low</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <article class="shop-element">
                        <div class="entry-image thumbnail-image"><img class="responsive-image"
                                                                      src="<?php echo $pictures['marsonry_blog_picture_one']; ?>"
                                                                      alt="marsonry_blog_picture_four"></div>

                        <div class="entry-item-rating">
                            <?php include "../elements/contents/blocks/item-rating.php"; ?>
                        </div>
                        <div class="entry-title entry-title-center"><h4>Lorem Ipsum Dolor Sit</h4></div>
                        <div class="entry-shop-details">
                            <div class="left"><a href="#">Add to cart</a></div>
                            <div class="right"><p>$5.5</p></div>
                        </div>
                        <div class="entry-more-details">
                            <a href="<?php echo !$html_or_php ? "single_product.php" : "single_product.html"; ?>">More Details</a>
                        </div>
                    </article>

                    <article class="shop-element">
                        <div class="entry-image thumbnail-image"><img class="responsive-image"
                                                                      src="<?php echo $pictures['marsonry_blog_picture_two']; ?>"
                                                                      alt="marsonry_blog_picture_four"></div>
                        <div class="entry-item-rating">
                            <?php include "../elements/contents/blocks/item-rating.php"; ?>
                        </div>
                        <div class="entry-title entry-title-center"><h4>Lorem Ipsum Dolor Sit</h4></div>
                        <div class="entry-shop-details">
                            <div class="left"><a href="#">Add to cart</a></div>
                            <div class="right"><p>$5.5</p></div>
                        </div>
                        <div class="entry-more-details">
                            <a href="<?php echo !$html_or_php ? "single_product.php" : "single_product.html"; ?>">More Details</a>
                        </div>
                    </article>

                    <article class="shop-element">
                        <div class="entry-image thumbnail-image"><img class="responsive-image"
                                                                      src="<?php echo $pictures['marsonry_blog_picture_three']; ?>"
                                                                      alt="marsonry_blog_picture_four"></div>
                        <div class="entry-item-rating">
                            <?php include "../elements/contents/blocks/item-rating.php"; ?>
                        </div>
                        <div class="entry-title entry-title-center"><h4>Lorem Ipsum Dolor Sit</h4></div>
                        <div class="entry-shop-details">
                            <div class="left"><a href="#">Add to cart</a></div>
                            <div class="right"><p>$5.5</p></div>
                        </div>
                        <div class="entry-more-details">
                            <a href="<?php echo !$html_or_php ? "single_product.php" : "single_product.html"; ?>">More Details</a>
                        </div>
                    </article>

                    <article class="shop-element">
                        <div class="entry-image thumbnail-image"><img class="responsive-image"
                                                                      src="<?php echo $pictures['marsonry_blog_picture_four']; ?>"
                                                                      alt="marsonry_blog_picture_four"></div>
                        <div class="entry-item-rating">
                            <?php include "../elements/contents/blocks/item-rating.php"; ?>
                        </div>
                        <div class="entry-title entry-title-center"><h4>Lorem Ipsum Dolor Sit</h4></div>
                        <div class="entry-shop-details">
                            <div class="left"><a href="#">Add to cart</a></div>
                            <div class="right"><p>$5.5</p></div>
                        </div>
                        <div class="entry-more-details">
                            <a href="<?php echo !$html_or_php ? "single_product.php" : "single_product.html"; ?>">More Details</a>
                        </div>
                    </article>

                    <article class="shop-element">
                        <div class="entry-image thumbnail-image"><img class="responsive-image"
                                                                      src="<?php echo $pictures['marsonry_blog_picture_five']; ?>"
                                                                      alt="marsonry_blog_picture_four"></div>
                        <div class="entry-item-rating">
                            <?php include "../elements/contents/blocks/item-rating.php"; ?>
                        </div>
                        <div class="entry-title entry-title-center"><h4>Lorem Ipsum Dolor Sit</h4></div>
                        <div class="entry-shop-details">
                            <div class="left"><a href="#">Add to cart</a></div>
                            <div class="right"><p>$5.5</p></div>
                        </div>
                        <div class="entry-more-details">
                            <a href="<?php echo !$html_or_php ? "single_product.php" : "single_product.html"; ?>">More Details</a>
                        </div>
                    </article>

                    <article class="shop-element">
                        <div class="entry-image thumbnail-image"><img class="responsive-image"
                                                                      src="<?php echo $pictures['marsonry_blog_picture_six']; ?>"
                                                                      alt="marsonry_blog_picture_four"></div>
                        <div class="entry-item-rating">
                            <?php include "../elements/contents/blocks/item-rating.php"; ?>
                        </div>
                        <div class="entry-title entry-title-center"><h4>Lorem Ipsum Dolor Sit</h4></div>
                        <div class="entry-shop-details">
                            <div class="left"><a href="#">Add to cart</a></div>
                            <div class="right"><p>$5.5</p></div>
                        </div>
                        <div class="entry-more-details">
                            <a href="<?php echo !$html_or_php ? "single_product.php" : "single_product.html"; ?>">More Details</a>
                        </div>
                    </article>

                    <article class="shop-element">
                        <div class="entry-image thumbnail-image"><img class="responsive-image"
                                                                      src="<?php echo $pictures['marsonry_blog_picture_seven']; ?>"
                                                                      alt="marsonry_blog_picture_four"></div>
                        <div class="entry-item-rating">
                            <?php include "../elements/contents/blocks/item-rating.php"; ?>
                        </div>
                        <div class="entry-title entry-title-center"><h4>Lorem Ipsum Dolor Sit</h4></div>
                        <div class="entry-shop-details">
                            <div class="left"><a href="#">Add to cart</a></div>
                            <div class="right"><p>$5.5</p></div>
                        </div>
                        <div class="entry-more-details">
                            <a href="<?php echo !$html_or_php ? "single_product.php" : "single_product.html"; ?>">More Details</a>
                        </div>
                    </article>

                    <article class="shop-element">
                        <div class="entry-image thumbnail-image"><img class="responsive-image"
                                                                      src="<?php echo $pictures['marsonry_blog_picture_eight']; ?>"
                                                                      alt="marsonry_blog_picture_four"></div>
                        <div class="entry-item-rating">
                            <?php include "../elements/contents/blocks/item-rating.php"; ?>
                        </div>
                        <div class="entry-title entry-title-center"><h4>Lorem Ipsum Dolor Sit</h4></div>
                        <div class="entry-shop-details">
                            <div class="left"><a href="#">Add to cart</a></div>
                            <div class="right"><p>$5.5</p></div>
                        </div>
                        <div class="entry-more-details">
                            <a href="<?php echo !$html_or_php ? "single_product.php" : "single_product.html"; ?>">More Details</a>
                        </div>
                    </article>

                    <article class="shop-element">
                        <div class="entry-image thumbnail-image"><img class="responsive-image"
                                                                      src="<?php echo $pictures['marsonry_blog_picture_nine']; ?>"
                                                                      alt="marsonry_blog_picture_four"></div>
                        <div class="entry-item-rating">
                            <?php include "../elements/contents/blocks/item-rating.php"; ?>
                        </div>
                        <div class="entry-title entry-title-center"><h4>Lorem Ipsum Dolor Sit</h4></div>
                        <div class="entry-shop-details">
                            <div class="left"><a href="#">Add to cart</a></div>
                            <div class="right"><p>$5.5</p></div>
                        </div>
                        <div class="entry-more-details">
                            <a href="<?php echo !$html_or_php ? "single_product.php" : "single_product.html"; ?>">More Details</a>
                        </div>
                    </article>

                    <article class="shop-element">
                        <div class="entry-image thumbnail-image"><img class="responsive-image"
                                                                      src="<?php echo $pictures['marsonry_blog_picture_ten']; ?>"
                                                                      alt="marsonry_blog_picture_four"></div>
                        <div class="entry-item-rating">
                            <?php include "../elements/contents/blocks/item-rating.php"; ?>
                        </div>
                        <div class="entry-title entry-title-center"><h4>Lorem Ipsum Dolor Sit</h4></div>
                        <div class="entry-shop-details">
                            <div class="left"><a href="#">Add to cart</a></div>
                            <div class="right"><p>$5.5</p></div>
                        </div>
                        <div class="entry-more-details">
                            <a href="<?php echo !$html_or_php ? "single_product.php" : "single_product.html"; ?>">More Details</a>
                        </div>
                    </article>

                </main>

            </div>

        </div>

    </div>

<?php include "../elements/html_footer.php"; ?>