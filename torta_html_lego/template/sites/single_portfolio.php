<?php include "../elements/html_head.php"; ?>

    <div class="container">

        <?php $heading = "Single Portfolio"; ?>
        <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>

        <div class="row">
            <div class="col-md-12">

                <main class="page-content-type">

                    <div class="single-portfolio<?php echo $scroll; ?>">
                        <div class="entry">
                            <div class="entry-image thumbnail-image">
                                <img class="responsive-image" src="<?php echo $pictures['blog_picture_one']; ?>"
                                     alt="blog_picture_one"/>
                            </div>
                            <div class="entry-details single-portfolio-entry-details">
            <span class="entry-details-author">
                <i class="fa fa-user"></i>
                By Admin
            </span>
            <span class="entry-details-date">
                <i class="fa fa-clock-o"></i>
                May 29th,2017
            </span>
            <span class="entry-details-comment">
                <i class="fa fa-comments-o"></i>
                5 Comments
            </span>
            <span class="entry-details-tag">
                <i class="fa fa-cloud"></i>
                Images, Photography
            </span>
                            </div>
                            <div class="entry-title">
                                <h2>Lorem ipsum</h2>
                            </div>
                            <div class="entry-single-portfolio">
                                <div class="row row-col-no-margin">
                                    <div class="col-md-9">
                                        <p>Eius posse melius et cum, idque molestiae mea ad, ut pri doctus qualisque
                                            suscipiantur.
                                            Nam alia ridens
                                            referrentur te, pri te primis dictas. His et vivendum verterem, deleniti
                                            lobortis
                                            liberavisse vim eu. Reque
                                            malis definiebas sed ne, vel ne stet affert. Vix debet vocibus civibus
                                            et, ei has
                                            nullam
                                            malorum.</p>
                                    </div>
                                    <div class="col-md-3">
                                        <ul class="ul-li-list ul-li-vertical-spacing-xs">
                                            <li>
                                                <ul class="ul-li-inline ul-li-horizontal-spacing-xs">
                                                    <li class="bold">Author by:</li>
                                                    <li>Adam Doe</li>
                                                </ul>
                                            </li>
                                            <li>
                                                <ul class="ul-li-inline ul-li-horizontal-spacing-xs">
                                                    <li class="bold">Client:</li>
                                                    <li>Envato</li>
                                                </ul>
                                            </li>
                                            <li>
                                                <ul class="ul-li-inline ul-li-horizontal-spacing-xs">
                                                    <li class="bold">Website:</li>
                                                    <li><a href="www.envato.com">www.envato.com</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <ul class="ul-li-inline ul-li-horizontal-spacing-xs">
                                                    <li class="bold">Share it:</li>
                                                    <li>
                                                        <div class="standard-social">
                                                            <a href="#" class="btn rs-btn-social">
                                                                <i class="fa fa-facebook fa-lg"></i>
                                                            </a>
                                                            <a href="#" class="btn rs-btn-social">
                                                                <i class="fa fa-twitter fa-lg"></i>
                                                            </a>
                                                            <a href="#" class="btn rs-btn-social">
                                                                <i class="fa fa-vimeo-square fa-lg"></i>
                                                            </a>
                                                            <a href="#" class="btn rs-btn-social">
                                                                <i class="fa fa-youtube-play fa-lg"></i>
                                                            </a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </main>

            </div>
        </div>

        <?php $heading = "Releated Portfolio"; ?>
        <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>

        <div class="row">
            <div class="col-md-12">
                <?php include "../elements/contents/blocks/releated_gallery.php"; ?>
            </div>
        </div>

    </div>

<?php include "../elements/html_footer.php"; ?>