<?php include "../elements/html_head.php"; ?>

    <div class="container">

        <?php $heading = "Single Post"; ?>
        <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>

        <div class="row">
            <div class="col-md-12">

                <main class="page-content-type">

                    <article class="post entry list-blog-element standard-post-format<?php echo $scroll; ?>">
                        <div class="entry-image thumbnail-image">
                            <img class="responsive-image" src="<?php echo $pictures['blog_picture_one']; ?>"
                                 alt="blog_picture_one"/>
                        </div>
                        <div class="entry-details list-blog-entry-details">
                            <span class="entry-details-author">
                                <i class="fa fa-user"></i>
                                By Admin
                            </span>
                            <span class="entry-details-date">
                                <i class="fa fa-clock-o"></i>
                                May 29th,2017
                            </span>
                            <span class="entry-details-comment">
                                <i class="fa fa-comments-o"></i>
                                5 Comments
                            </span>
                            <span class="entry-details-tag">
                                <i class="fa fa-cloud"></i>
                                Images, Photography
                            </span>
                        </div>
                        <div class="entry-title">
                            <h2>Standard blog post</h2>
                        </div>
                        <div class="entry-excerpt">
                            <p>Feugiat donec dui eu, dignissim lacinia risus porttitor donec viverra bibendum, habitasse
                                pellentesque ac
                                mauris, eget sem consectetuer. Pretium lorem at ridiculus molestie, eu eu volutpat
                                hendrerit egestas
                                wisi
                                ut, nulla turpis interdum repellat, purus fusce. Elit amet mauris in sagittis, at dui
                                nam massa
                                metus et
                                nulla, vestibulum vestibulum suspendisse, vel id arcu quam quisque pede tristique,
                                litora ut ipsum.
                                Mi cras,
                                mi nulla ac eros, tellus quis sollicitudin ante libero, hendrerit in elit eleifend vel
                                rhoncus
                                risus. Amet
                                luctus, est ullamcorper, aliquet tristique etiam habitasse sed augue in, vitae velit.
                                Tellus tortor
                                quam
                                nullam eros, fermentum accusantium est mauris vitae potenti, primis montes. Vitae urna
                                blandit
                                etiam,
                                pulvinar volutpat nibh felis viverra, orci et morbi tincidunt blandit, erat nisl nulla
                                urna.
                                Nam integer feugiat eget diam quis, rutrum sit, non risus, mauris at. Eget ligula
                                sapien, non massa
                                ante
                                vivamus augue occaecat erat, viverra sed sapien. Nunc felis senectus est bibendum,
                                tincidunt pede
                                wisi pede
                                sed commodo. Quam wisi, tempus erat. Augue vitae, eget nibh libero adipiscing. In sem,
                                magnis justo
                                dui
                                proin nulla, est vestibulum natoque ut, nunc pulvinar massa quam feugiat faucibus quis,
                                vestibulum
                                aliquam.
                                Elementum consequat sed nisl, ipsum parturient quis. Volutpat lacus dictum dui proin,
                                proident
                                malesuada
                                cras. Tempor nec est purus. Nonummy suscipit suspendisse luctus lacus enim. Imperdiet
                                magna, arcu
                                non diam,
                                id in nunc congue sed proin aliquam.</p>
                        </div>
                    </article>
                    <?php include "../elements/contents/blocks/comments.php"; ?>

                </main>

            </div>
        </div>

    </div>

<?php include "../elements/html_footer.php"; ?>