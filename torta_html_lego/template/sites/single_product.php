<?php include "../elements/html_head.php"; ?>

<div class="container">

    <?php $heading = "Single Product"; ?>
    <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>

    <main class="page-content-type">

        <div class="row">

            <div class="col-md-5">

                <article class="product entry">
                    <img class="responsive-image" src="<?php echo $pictures['single_product_one']; ?>" alt="single_product_one">

                </article>

            </div>

            <div class="col-md-7">


                <article class="single-product">

                    <div class="single-product-title">
                        <h3 class="no-margin">Lorem ipsum</h3>
                    </div>
                    <div class="single-product-price">
                        <h3 class="no-margin positive">$35.00</h3>
                    </div>
                    <div class="single-product-text">
                        <p class="no-margin">Pellentesque habitant morbi tristique senectus et netus et malesuada fames
                            ac turpis
                            egestas. Vestibulum
                            tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit
                            amet quam egestas
                            semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
                    </div>
                    <div class="single-product-quantity">
                        <div class="large-margin">
                            <ul class="ul-li-inline ul-li-horizontal-spacing-xs">
                                <li><a class="cart-negative " href="#">-</a></li>
                                <li><input type="text" class="form-control cart-input text-center" value="1"></li>
                                <li><a class="cart-positive " href="#">+</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="single-product-read-more">
                        <a href="#" class="btn btn-primary">Add to cart</a>
                    </div>
                    <div class="single-product-categories">
                        <ul class="ul-li-inline ul-li-horizontal-spacing-xs">
                            <li class="bold">Categories:</li>
                            <li><a href="#">Muffin</a></li>
                            <li>/</li>
                            <li><a href="#">Cake</a></li>
                        </ul>
                    </div>
                    <div class="single-product-tags">
                        <ul class="ul-li-inline ul-li-horizontal-spacing-xs">
                            <li class="bold">Tag:</li>
                            <li><a href="#">Muffin</a></li>
                            <li>/</li>
                            <li><a href="#">Cake</a></li>
                        </ul>
                    </div>
                    <div class="single-product-cards">
                        <div class="card">
                            <ul class="nav nav-tabs ul-li-no-padding" role="tablist">
                                <li role="presentation" class="active"><a href="#description"
                                                                          aria-controls="description" role="tab"
                                                                          data-toggle="tab">Description</a></li>
                                <li role="presentation"><a href="#reviews" aria-controls="reviews" role="tab"
                                                           data-toggle="tab">Reviews
                                        (4)</a></li>
                            </ul>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="description">
                                    <h3>Product Description</h3>

                                    <p class="no-margin">Lorem Ipsum is simply dummy text of the
                                        printing and typesetting industry. Lorem Ipsum has been the industry's
                                        standard dummy text
                                        ever
                                        since the 1500s, when an unknown printer took a galley of type and scrambled
                                        it to make a
                                        type
                                        specimen book. It has survived not only five centuries, but also the leap
                                        into electronic
                                        typesetting, remaining essentially unchanged. It was popularised in the
                                        1960s with the
                                        release
                                        of Letraset sheets containing Lorem Ipsum passages, and more recently with
                                        desktop
                                        publishing
                                        software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="reviews">
                                    <?php include "../elements/contents/blocks/comments.php"; ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </article>

            </div>

        </div>

        <?php $heading = "Latest Products"; ?>
        <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>

        <?php include '../elements/contents/blocks/latest_products.php'; ?>

    </main>

</div>

<?php include "../elements/html_footer.php"; ?>



