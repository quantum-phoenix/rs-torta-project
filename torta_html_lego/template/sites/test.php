<?php include "../elements/html_head.php"; ?>

    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <?php $heading = "Blog With Sidebar"; ?>
                <?php include '../elements/contents/blocks/heading_variation_one.php'; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <main class="page-content-type">

                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt, tempore?</p>

                </main>

            </div>
        </div>

    </div>

<?php include "../elements/html_footer.php"; ?>