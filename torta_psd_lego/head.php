<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Torta</title>	
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
	<style>
		img{
			height: auto;
			max-width: 100%;
		}
		body{
			background-color: #ccc;
		}
	</style>
</head>
<body>
<ul class="list-inline">
	<li class="list-group-item"><a href="home_v_1.php">Home v 1</a></li>
	<li class="list-group-item"><a href="shop_with_sidebar.php">Shop with sidebar</a></li>
	<li class="list-group-item"><a href="shop_without_sidebar.php">Shop without sidebar</a></li>	
	<li class="list-group-item"><a href="about_us.php">About Us</a></li>
	<li class="list-group-item"><a href="blog_with_sidebar.php">Blog with sidebar</a></li>
	<li class="list-group-item"><a href="blog_without_sidebar.php">Blog without sidebar</a></li>
</ul>