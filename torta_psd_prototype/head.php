<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Torta</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <style>
      img {
        height: auto;
        max-width: 100%;
        display: block;
      }
      body {
        background-color: #ccc;
      }
      @media (min-width: 1200px) {
        .container {
          width: 1060px;
        }
      }
    </style>
  </head>
  <body>
    <ul class="list-inline">
      <li class="list-group-item">
        <a href="home_v1.php">Home v1</a>
      </li>
      <li class="list-group-item">
        <a href="shop_with_sidebar.php">Shop with sidebar</a>
      </li>
      <li class="list-group-item">
        <a href="blog_with_sidebar.php">Blog with sidebar</a>
      </li>
      <li class="list-group-item">
        <a href="aboutus_v1.php">About Us</a>
      </li>      
      <li class="list-group-item">
        <a href="contact_v1.php">Contact</a>
      </li>
      <li class="list-group-item">
        <a href="single_product_v1.php">Single product</a>
      </li>
      <li class="list-group-item">
        <a href="single_blog_v1.php">Single blog</a>
      </li>
      <li class="list-group-item">
        <a href="shoping_cart_v1.php">Shoping cart</a>
      </li>
      <li class="list-group-item">
        <a href="checkout_v1.php">Checkout</a>
      </li>
	  <li class="list-group-item">
        <a href="menu_v1.php">Menu</a>
      </li>
    </ul>
